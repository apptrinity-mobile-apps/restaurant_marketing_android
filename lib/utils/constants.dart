
import 'dart:ui';


/* all strings goes here */
const String appName = "Zing Marketing";
const String offersPromotionsLine1 = "Send Offer Deals";
const String offersPromotionsLine2 = "Or Promotions";
const String view = "view";
const String socialMediaLine1 = "Send Social Media";
const String socialMediaLine2 = "Posts";
const String shareStoreLink = "Share Store Link";
const String getCatalogWhatsApp = "Get Catalog on WhatApp";
const String getStoreQr = "Get Store QR";
const String restaurantName = "Restaurant Name";
const String shareFullCatalogWhatsApp =
    "Share your full catalog as pdf on WhatsApp";
const String getFullCatalog = "GET MY FULL CATALOG";
const String howItWorks = "How it works?";
const String sendFullCatalogWhatsApp =
    "We will send your full catalog pdf on your WhatsApp.";
const String forwardToCustomers = "Forward it to all your customers.";
const String headerMyQr = "My QR Code";
const String headerBusinessCreative = "Business Creatives";
const String headerEditShare = "Edit & Share";
const String howToUse = "How to use this?";
const String saveQr = "SAVE QR";
const String saveDevice = "SAVE TO DEVICE";
const String printQrToStore = "Get print out & paste the QR in your store.";
const String scanQrToOpenStore =
    "Ask customers to Scan QR to open your digital showroom.";
const String customersViewPlaceOrder =
    "Customers can view your online store & place their order online.";
const String newLaunchesBestSellers = "New Launches & Bestsellers";
const String template = "Template";
const String productDiscount = "Product Discount";
const String browseOffersTemplates = "Browse offers & deals templates";
const String edit = "Edit";
const String share = "Share";
const String whatsApp = "WhatsApp";
const String scanOrder = "SCAN > ORDER > PAY";
const String editText = "Edit Text";
const String background = "Background";
const String editBackground = "Edit Background";
const String topLine = "Top Line";
const String middleLine = "Middle Line";
const String bottomLine = "Bottom Line";
const String saveChanges = "SAVE CHANGES";
const String bestSeller = "Best Seller";
const String changeProduct = "Change product";
const String availableItems = "Available Items";
const String warningProduct1 = "Please Note ";
const String warningProduct2 = "only products with images are shown here.";
const String searchProducts = "Search Products";
const String socialMediaPosts = "Social Media Posts";
const String mFav = "Fav";
const String favorites = "Favorites";
const String userName = "User Name";
const String password = "Password";
const String login = "Login";
const String invalidUserName = "Invalid username!";
const String emptyUserName = "Please enter Username!";
const String invalidPassword = "Invalid password";
const String emptyPassword = "Please enter password";
const String loginSuccess = "Login Successful";
const String logoutPassword = "Logout Successful";
const String digitalShowroom = "digital SHOWROOM";
const String scanQR = "Scan QR";
const String pleaseWait = "Please wait...";
const String tryAgain = "Please try again...";
const String waitSaving = "Saving please wait...";
const String saved = "Saved to device.";
const String update = "Update";
const String unableToShareWhatsApp = "Couldn't share to WhatsApp, WhatsApp is unavailable!";
const String noDataAvailable = "No data available!";


/* all colors goes here */
const Color favourite_color = Color(0xffEF4B3D);
const Color un_favourite_color = Color(0xffD8DFED);
const Color qr_bg_color_red = Color(0xffFF7266);
const Color qr_bg_color_yellow = Color(0xffFFCA28);
const Color qr_bg_color_blue = Color(0xff64B5F6);
const Color qr_bg_color_green_shade = Color(0xff1EE1C3);