bool validatePassword(String value){
  // String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  String  pattern = r'.{6,}';
  RegExp regExp = new RegExp(pattern);
  return regExp.hasMatch(value);
}