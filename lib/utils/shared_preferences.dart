import 'dart:convert';
import 'package:marketing/ui/login/model/login_response_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  static const String userLoggedIn = "isLoggedIn";
  static const String userLoginDetails = "user_login_details";

  Future<bool?> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(userLoggedIn) == null ? false : true;
  }

  void login(bool isLogin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(userLoggedIn, isLogin);
  }

  void saveUserDetails(userDetails) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(userLoginDetails, userDetails);
  }

  Future<LoginResponse> getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(userLoginDetails) ?? "";
    LoginResponse _loginResponse = LoginResponse.fromJson(jsonDecode(_details));
    return _loginResponse;
  }
}
