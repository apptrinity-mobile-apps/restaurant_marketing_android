import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Widget loader() {
  return SpinKitCircle(
    color: Colors.lightBlueAccent,
    size: 50.0,
  );
}
