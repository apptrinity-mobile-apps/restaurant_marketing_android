import 'package:flutter/material.dart';

class ScreenSize {
  static Size size(BuildContext c) => MediaQuery.of(c).size;
  static double width(BuildContext c) => MediaQuery.of(c).size.width;
  static double height(BuildContext c) => MediaQuery.of(c).size.height;
  static double viewInsetsBottom(BuildContext c) => MediaQuery.of(c).viewInsets.bottom;
  static double viewInsetsTop(BuildContext c) => MediaQuery.of(c).viewInsets.top;
  static double pixelRatio(BuildContext c) => MediaQuery.of(c).devicePixelRatio;
}
