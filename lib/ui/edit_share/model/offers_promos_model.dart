
class OffersPromosResponse {
  late OffersAndPromotionsData offersAndPromotionsData;
  late int responseStatus;
  late String result;

  OffersPromosResponse(
      {required this.offersAndPromotionsData,
      required this.responseStatus,
      required this.result});

  OffersPromosResponse.fromJson(Map<String, dynamic> json) {
    offersAndPromotionsData = (json['offersAndPromotionsData'] != null
        ? new OffersAndPromotionsData.fromJson(json['offersAndPromotionsData'])
        : null)!;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['offersAndPromotionsData'] = this.offersAndPromotionsData.toJson();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class OffersAndPromotionsData {
  late String bottomText;
  late bool bottomTextCheck;
  late String bottomTextLength;
  late String createdOn;
  late String defaultBackgroundImage;
  late String id;
  late String image;
  late String middleImage;
  late String middleText;
  late bool middleTextCheck;
  late String middleTextLength;
  late List<MultipleBackgroundImages> multipleBackgroundImages;
  late bool restaurantImageCheck;
  late String restaurantMaxWidth;
  late String restaurantMinWidth;
  late int status;
  late String topText;
  late bool topTextCheck;
  late String topTextLength;

  OffersAndPromotionsData(
      {required this.bottomText,
      required this.bottomTextCheck,
      required this.bottomTextLength,
      required this.createdOn,
      required this.defaultBackgroundImage,
      required this.id,
      required this.image,
      required this.middleImage,
      required this.middleText,
      required this.middleTextCheck,
      required this.middleTextLength,
      required this.multipleBackgroundImages,
      required this.restaurantImageCheck,
      required this.restaurantMaxWidth,
      required this.restaurantMinWidth,
      required this.status,
      required this.topText,
      required this.topTextCheck,
      required this.topTextLength});

  OffersAndPromotionsData.fromJson(Map<String, dynamic> json) {
    bottomText = json['bottomText'];
    bottomTextCheck = json['bottomTextCheck'];
    bottomTextLength = json['bottomTextLength'];
    createdOn = json['createdOn'];
    defaultBackgroundImage = json['defaultBackgroundImage'];
    id = json['id'];
    image = json['image'];
    middleImage = json['middleImage'];
    middleText = json['middleText'];
    middleTextCheck = json['middleTextCheck'];
    middleTextLength = json['middleTextLength'];
    if (json['multipleBackgroundImages'] != null) {
      multipleBackgroundImages = <MultipleBackgroundImages>[];
      json['multipleBackgroundImages'].forEach((v) {
        multipleBackgroundImages.add(new MultipleBackgroundImages.fromJson(v));
      });
    }
    restaurantImageCheck = json['restaurantImageCheck'];
    restaurantMaxWidth = json['restaurantMaxWidth'];
    restaurantMinWidth = json['restaurantMinWidth'];
    status = json['status'];
    topText = json['topText'];
    topTextCheck = json['topTextCheck'];
    topTextLength = json['topTextLength'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bottomText'] = this.bottomText;
    data['bottomTextCheck'] = this.bottomTextCheck;
    data['bottomTextLength'] = this.bottomTextLength;
    data['createdOn'] = this.createdOn;
    data['defaultBackgroundImage'] = this.defaultBackgroundImage;
    data['id'] = this.id;
    data['image'] = this.image;
    data['middleImage'] = this.middleImage;
    data['middleText'] = this.middleText;
    data['middleTextCheck'] = this.middleTextCheck;
    data['middleTextLength'] = this.middleTextLength;
    data['multipleBackgroundImages'] =
        this.multipleBackgroundImages.map((v) => v.toJson()).toList();
    data['restaurantImageCheck'] = this.restaurantImageCheck;
    data['restaurantMaxWidth'] = this.restaurantMaxWidth;
    data['restaurantMinWidth'] = this.restaurantMinWidth;
    data['status'] = this.status;
    data['topText'] = this.topText;
    data['topTextCheck'] = this.topTextCheck;
    data['topTextLength'] = this.topTextLength;
    return data;
  }
}

class MultipleBackgroundImages {
  late String image;

  MultipleBackgroundImages({required this.image});

  MultipleBackgroundImages.fromJson(Map<String, dynamic> json) {
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    return data;
  }
}
