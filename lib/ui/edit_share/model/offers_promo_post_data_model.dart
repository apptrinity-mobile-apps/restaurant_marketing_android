import 'package:marketing/ui/edit_share/model/offers_promos_model.dart';

class OffersAndPromoPostData {
  late String offersPromotionsId;
  late String topText;
  late String middleText;
  late String bottomText;
  late String userId;
  late String defaultBackgroundImage;
  late String middleImage;
  late List<MultipleBackgroundImages> multipleBackgroundImages;
  late String topTextLength;
  late String middleTextLength;
  late String bottomTextLength;
  late bool topTextCheck;
  late bool middleTextCheck;
  late bool bottomTextCheck;
  late String restaurantCustomImage;
  late String restaurantMaxWidth;
  late bool restaurantImageCheck;
  late String restaurantMinWidth;

  OffersAndPromoPostData(
      {required this.offersPromotionsId,
        required this.topText,
        required this.bottomText,
        required this.middleText,
        required this.userId,
        required this.defaultBackgroundImage,
        required this.middleImage,
        required this.multipleBackgroundImages,
        required this.topTextLength,
        required this.middleTextLength,
        required this.bottomTextLength,
        required this.topTextCheck,
        required this.middleTextCheck,
        required this.bottomTextCheck,
        required this.restaurantCustomImage,
        required this.restaurantMaxWidth,
        required this.restaurantImageCheck,
        required this.restaurantMinWidth});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['offersPromotionsId'] = this.offersPromotionsId;
    data['topText'] = this.topText;
    data['middleText'] = this.middleText;
    data['bottomText'] = this.bottomText;
    data['userId'] = this.userId;
    data['defaultBackgroundImage'] = this.defaultBackgroundImage;
    data['middleImage'] = this.middleImage;
    data['multipleBackgroundImages'] = this.multipleBackgroundImages.map((v) => v.toJson()).toList();
    data['topTextLength'] = this.topTextLength;
    data['middleTextLength'] = this.middleTextLength;
    data['bottomTextLength'] = this.bottomTextLength;
    data['topTextCheck'] = this.topTextCheck;
    data['middleTextCheck'] = this.middleTextCheck;
    data['bottomTextCheck'] = this.bottomTextCheck;
    data['restaurantCustomImage'] = this.restaurantCustomImage;
    data['restaurantMaxWidth'] = this.restaurantMaxWidth;
    data['restaurantImageCheck'] = this.restaurantImageCheck;
    data['restaurantMinWidth'] = this.restaurantMinWidth;
    return data;
  }
}
