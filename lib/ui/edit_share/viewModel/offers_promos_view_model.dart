import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:marketing/services/basic_response.dart';
import 'package:marketing/services/repositories.dart';
import 'package:marketing/ui/edit_share/model/offers_promo_post_data_model.dart';
import 'package:marketing/ui/edit_share/model/offers_promos_model.dart';

class PromosOffersViewModel with ChangeNotifier {
  PromosOffersViewModel();

  bool _isFetching = false, _isHavingOfferAndPromo = false, _hasUpdate = false;
  OffersPromosResponse? _offersPromosResponse;
  BasicResponse? _basicResponse;

  bool get isFetching => _isFetching;
  bool get hasUpdate => _hasUpdate;

  bool get isHavingOfferAndPromo => _isHavingOfferAndPromo;

  OffersPromosResponse? get offersPromosResponse {
    return _offersPromosResponse;
  }

  BasicResponse? get updateResponse {
    return  _basicResponse;
  }

  Future<void> getAllPromosDetails(
      String offersPromotionsId, String employeeId, String restaurantId) async {
    _isFetching = true;
    _isHavingOfferAndPromo = false;
    _offersPromosResponse = null;
    notifyListeners();
    try {
      dynamic response = await Repository()
          .getOfferPromoDetails(offersPromotionsId, employeeId, restaurantId);
      if (response != null) {
        OffersPromosResponse _response =
            OffersPromosResponse.fromJson(response);
        _offersPromosResponse = _response;
        _isHavingOfferAndPromo = true;
      } else {
        _isHavingOfferAndPromo = false;
      }
    } catch (e) {
      print("error $e");
      _isHavingOfferAndPromo = false;
    }
    _isFetching = false;
    notifyListeners();
  }

  Future<bool> savePromoApi(OffersAndPromoPostData obj) async {
    _isFetching = true;
    _hasUpdate = false;
    _basicResponse = null;
    notifyListeners();
    try {
      dynamic response = await Repository().updatePromoOffer(obj);
      if (response != null) {
        BasicResponse _response = BasicResponse.fromJson(response);
        _basicResponse = _response;
        _hasUpdate = true;
      } else {
        _basicResponse = null;
        _hasUpdate = false;
      }
    } catch (e) {
      _basicResponse = null;
      _hasUpdate = false;
      print("error $e");
    }
    _isFetching = false;
    notifyListeners();
    return _hasUpdate;
  }
}
