import 'dart:collection';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:marketing/services/basic_response.dart';
import 'package:marketing/ui/edit_share/model/offers_promo_post_data_model.dart';
import 'package:marketing/ui/edit_share/model/offers_promos_model.dart';
import 'package:marketing/ui/edit_share/viewModel/offers_promos_view_model.dart';
import 'package:marketing/ui/edit_share/widgets/bottom_sheet_edit_backgrounds.dart';
import 'package:marketing/ui/edit_share/widgets/bottom_sheet_edit_texts.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:marketing/utils/snack_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class EditAndShareScreen extends StatefulWidget {
  final String offerId;

  const EditAndShareScreen({required this.offerId}) : super();

  @override
  _EditAndShareScreenState createState() => _EditAndShareScreenState();
}

class _EditAndShareScreenState extends State<EditAndShareScreen> {
  String id = "",
      mRestaurantId = "",
      mEmployeeId = "",
      mDefaultImage = "",
      mTopText = "",
      mMiddleText = "",
      mBottomText = "";
  bool isDataLoaded = true;
  List<String> imagePaths = [];
  ScreenshotController screenshotController = ScreenshotController();
  late BuildContext dialogContext;
  OffersAndPromotionsData? _offersAndPromotionsData;

  @override
  void initState() {
    setState(() {
      id = widget.offerId;
    });
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mRestaurantId = value.employeeDetails.restaurantId;
        mEmployeeId = value.employeeDetails.id;
        isDataLoaded = false;
        Provider.of<PromosOffersViewModel>(context, listen: false)
            .getAllPromosDetails(id, mEmployeeId, mRestaurantId);
      });
    });
    super.initState();
  }

  Widget promosOffersDetailView(PromosOffersViewModel viewModel) {
    bool havingData = viewModel.isHavingOfferAndPromo ? true : false;
    bool hasData = havingData
        ? (viewModel.offersPromosResponse!.responseStatus == 1 ? true : false)
        : false;
    if (viewModel.offersPromosResponse != null) {
      _offersAndPromotionsData =
          viewModel.offersPromosResponse!.offersAndPromotionsData;
      if (!isDataLoaded) {
        setState(() {
          mDefaultImage = _offersAndPromotionsData!.defaultBackgroundImage;
          mTopText = _offersAndPromotionsData!.topText;
          mMiddleText = _offersAndPromotionsData!.middleText;
          mBottomText = _offersAndPromotionsData!.bottomText;
          isDataLoaded = true;
        });
      }
    }
    return havingData
        ? (hasData
        ? new Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: ScreenSize.height(context) / 1.7,
            width: ScreenSize.width(context),
            margin: EdgeInsets.all(10),
            child: Screenshot(
                controller: screenshotController,
                child: Card(
                  elevation: 3,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Column(
                        children: [
                          Expanded(
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  CachedNetworkImage(
                                    placeholder: (context, url) =>
                                        loader(),
                                    imageUrl: mDefaultImage,
                                    errorWidget: (context, url, error) =>
                                        Icon(
                                          Icons.error_outline,
                                          size: 50,
                                        ),
                                    fit: BoxFit.fill,
                                    filterQuality: FilterQuality.medium,
                                  ),
                                  Align(
                                    alignment: Alignment.center,
                                    child: _offersAndPromotionsData!
                                        .middleImage ==
                                        ""
                                        ? SizedBox()
                                        : Container(
                                      height: ScreenSize.height(
                                          context) /
                                          5,
                                      width: ScreenSize.width(
                                          context) /
                                          2,
                                      padding: EdgeInsets.all(5),
                                      child: CachedNetworkImage(
                                        placeholder:
                                            (context, url) =>
                                            loader(),
                                        imageUrl:
                                        _offersAndPromotionsData!
                                            .middleImage,
                                        errorWidget:
                                            (context, url, error) =>
                                            Icon(
                                              Icons.error_outline,
                                              size: 50,
                                            ),
                                        fit: BoxFit.fill,
                                        filterQuality:
                                        FilterQuality.medium,
                                      ),
                                    ),
                                  ),
                                  _offersAndPromotionsData!.topTextCheck
                                      ? Positioned(
                                      top: 30,
                                      left: 0,
                                      right: 0,
                                      child: Align(
                                        alignment:
                                        Alignment.topCenter,
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          child: AutoSizeText(
                                            mTopText,
                                            maxLines: 1,
                                            minFontSize: 14,
                                            overflow:
                                            TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color:
                                                Color(0xffF3F5F8),
                                                fontSize: 16,
                                                fontFamily: "Poppins",
                                                fontWeight:
                                                FontWeight.w600),
                                          ),
                                        ),
                                      ))
                                      : SizedBox(),
                                  _offersAndPromotionsData!
                                      .middleTextCheck
                                      ? Positioned(
                                      top: 0,
                                      left: 0,
                                      right: 0,
                                      bottom: 0,
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          child: AutoSizeText(
                                            mMiddleText,
                                            maxLines: 1,
                                            minFontSize: 14,
                                            overflow:
                                            TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color:
                                                Color(0xffF3F5F8),
                                                fontSize: 16,
                                                fontFamily: "Poppins",
                                                fontWeight:
                                                FontWeight.w600),
                                          ),
                                        ),
                                      ))
                                      : SizedBox(),
                                  _offersAndPromotionsData!
                                      .bottomTextCheck
                                      ? Positioned(
                                      top: 0,
                                      left: 0,
                                      right: 0,
                                      bottom: 30,
                                      child: Align(
                                        alignment:
                                        Alignment.bottomCenter,
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          child: AutoSizeText(
                                            mBottomText,
                                            maxLines: 1,
                                            minFontSize: 14,
                                            overflow:
                                            TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color:
                                                Color(0xffF3F5F8),
                                                fontSize: 16,
                                                fontFamily: "Poppins",
                                                fontWeight:
                                                FontWeight.w600),
                                          ),
                                        ),
                                      ))
                                      : SizedBox(),
                                ],
                              )),
                          Stack(
                            fit: StackFit.loose,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        5, 20, 15, 20),
                                    child: Row(
                                      children: [
                                        Expanded(child: Container()),
                                        Expanded(
                                          child: AutoSizeText(
                                            scanOrder,
                                            maxLines: 2,
                                            minFontSize: 14,
                                            textAlign:
                                            TextAlign.start,
                                            style: TextStyle(
                                                color:
                                                Color(0xff3D4B65),
                                                fontSize: 18,
                                                fontFamily: "Poppins",
                                                fontWeight:
                                                FontWeight.w600),
                                          ),
                                          flex: 2,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        5, 15, 15, 15),
                                    color: Color(0xff64B5F6),
                                    child: Row(
                                      children: [
                                        Expanded(child: Container()),
                                        Expanded(
                                          child: AutoSizeText(
                                            "https://f-Mama's Fish House.in",
                                            maxLines: 2,
                                            minFontSize: 14,
                                            textAlign:
                                            TextAlign.start,
                                            style: TextStyle(
                                                color:
                                                Color(0xffF6F8FB),
                                                fontSize: 14,
                                                fontFamily: "Poppins",
                                                fontWeight:
                                                FontWeight.w400),
                                          ),
                                          flex: 2,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Positioned(
                                left: 15,
                                top: 15,
                                bottom: 15,
                                child: Container(
                                  color: Colors.white,
                                  padding: EdgeInsets.all(5),
                                  child: Image.asset(
                                    "assets/images/qr_code.png",
                                    height: 80,
                                    width: 80,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )),
          ),
        ])
        : Center(
      child: AutoSizeText(
        noDataAvailable,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
      ),
    ))
        : Container(
      child: loader(),
    );
  }

  void updatePromo(PromosOffersViewModel viewModel) {
    BasicResponse? response;
    bool havingData = viewModel.hasUpdate;
    print(viewModel.updateResponse!.result);
    if (viewModel.updateResponse != null) {
      setState(() {
        response = viewModel.updateResponse;
      });
    }
    Navigator.pop(dialogContext);
    if (havingData) {
      if (response!.responseStatus == 1) {
        viewModel.getAllPromosDetails(id, mEmployeeId, mRestaurantId);
        showSnackBar(context, viewModel.updateResponse!.result);
      } else {
        showSnackBar(context, viewModel.updateResponse!.result);
      }
    } else {
      showSnackBar(context, tryAgain);
    }
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<PromosOffersViewModel>(context);
    return Scaffold(
      backgroundColor: Color(0xffF6F8FB),
      appBar: AppBar(
        leading: InkWell(
          customBorder: new CircleBorder(),
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back),
        ),
        backgroundColor: Color(0xff293855),
        title: Text(
          headerEditShare,
          style: TextStyle(
              fontSize: 18, fontFamily: "Poppins", fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        toolbarHeight: 56,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0x80101621),
            statusBarIconBrightness: Brightness.light),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          color: Colors.black12,
          child: Column(
            children: [
              promosOffersDetailView(viewModel),
              Container(
                margin: EdgeInsets.all(10),
                alignment: Alignment.center,
                child: Card(
                  elevation: 3,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: InkWell(
                    onTap: () async {
                      if (_offersAndPromotionsData != null) {
                        showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) {
                              dialogContext = context;
                              return loader();
                            });
                        var obj = OffersAndPromoPostData(
                            offersPromotionsId: id,
                            topText: mTopText,
                            middleText: mMiddleText,
                            bottomText: mBottomText,
                            userId: mEmployeeId,
                            defaultBackgroundImage: mDefaultImage,
                            middleImage: _offersAndPromotionsData!.middleImage,
                            multipleBackgroundImages: _offersAndPromotionsData!
                                .multipleBackgroundImages,
                            topTextLength:
                            _offersAndPromotionsData!.topTextLength,
                            middleTextLength:
                            _offersAndPromotionsData!.middleTextLength,
                            bottomTextLength:
                            _offersAndPromotionsData!.bottomTextLength,
                            topTextCheck:
                            _offersAndPromotionsData!.topTextCheck,
                            middleTextCheck:
                            _offersAndPromotionsData!.middleTextCheck,
                            bottomTextCheck:
                            _offersAndPromotionsData!.bottomTextCheck,
                            restaurantCustomImage: "",
                            restaurantMaxWidth:
                            _offersAndPromotionsData!.restaurantMaxWidth,
                            restaurantImageCheck:
                            _offersAndPromotionsData!.restaurantImageCheck,
                            restaurantMinWidth:
                            _offersAndPromotionsData!.restaurantMinWidth);
                        obj.toJson();
                        print("object=====${jsonEncode(obj)}=====");
                        await viewModel.savePromoApi(obj).then((value) {
                            updatePromo(viewModel);
                        });
                      } else {
                        showSnackBar(context, "Please wait till data loads.");
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: AutoSizeText(
                        update,
                        maxLines: 3,
                        minFontSize: 14,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xff3D4B65),
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomSheet: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            children: [
              Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: Material(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              showModalBottomSheet(
                                  context: context,
                                  backgroundColor: Colors.white,
                                  enableDrag: false,
                                  isDismissible: true,
                                  isScrollControlled: true,
                                  useRootNavigator: true,
                                  builder: (context) {
                                    return new BottomSheetEditTextsWidgets();
                                  }).then((value) {
                                var values = value == null ? HashMap() : value;
                                log("message $values");
                                setState(() {
                                  mTopText = values["top"] == null
                                      ? ""
                                      : values["top"];
                                  mMiddleText = values["middle"] == null
                                      ? ""
                                      : values["middle"];
                                  mBottomText = values["bottom"] == null
                                      ? ""
                                      : values["bottom"];
                                  log(
                                      "message $mTopText  $mMiddleText  $mBottomText");
                                });
                              });
                            },
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Image.asset(
                                    "assets/images/edit_text_icon.png",
                                    height: 60,
                                    width: 60,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: AutoSizeText(
                                    editText,
                                    style: TextStyle(
                                        color: Color(0xff3D4B65),
                                        fontSize: 13,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                backgroundColor: Colors.white,
                                enableDrag: true,
                                isDismissible: true,
                                isScrollControlled: true,
                                useRootNavigator: true,
                                builder: (context) {
                                  return new BottomSheetEditBackgroundWidget(
                                    multipleBackgroundImages: viewModel
                                        .offersPromosResponse!
                                        .offersAndPromotionsData
                                        .multipleBackgroundImages,
                                  );
                                },
                              ).then((value) {
                                var image = value == null ? "" : value;
                                if (image != "")
                                  setState(() {
                                    mDefaultImage = image;
                                  });
                              });
                            },
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Image.asset(
                                    "assets/images/edit_background_icon.png",
                                    height: 60,
                                    width: 60,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: AutoSizeText(
                                    background,
                                    style: TextStyle(
                                        color: Color(0xff3D4B65),
                                        fontSize: 13,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () async {
                              systemShare();
                            },
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Image.asset(
                                    "assets/images/share_product_rounded.png",
                                    height: 60,
                                    width: 60,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: AutoSizeText(
                                    share,
                                    style: TextStyle(
                                        color: Color(0xff3D4B65),
                                        fontSize: 13,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              whatsappShare();
                            },
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Image.asset(
                                    "assets/images/share_whatsapp_black_rounded.png",
                                    height: 60,
                                    width: 60,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: AutoSizeText(
                                    whatsApp,
                                    style: TextStyle(
                                        color: Color(0xff3D4B65),
                                        fontSize: 13,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ],
      ),
    );
  }

  systemShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
        pixelRatio: ScreenSize.pixelRatio(context),
        delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      setState(() {
        imagePaths.clear();
        imagePaths.add(imgFile.path);
      });
      final RenderBox box = context.findRenderObject() as RenderBox;
      Share.shareFiles(imagePaths,
          text: "Sharing image",
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });
  }

  whatsappShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
        pixelRatio: ScreenSize.pixelRatio(context),
        delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      FlutterShareMe()
          .shareToWhatsApp(
          msg: "Sharing image",
          imagePath: imgFile.path,
          fileType: FileType.image)
          .then((value) {
        if (value!.toString().contains("PlatformException")) {
          showSnackBar(context, unableToShareWhatsApp);
        }
      });
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });
  }
}
