import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marketing/utils/progress_loading.dart';

class ItemBackgroundWidget extends StatefulWidget {
  final String image;

  const ItemBackgroundWidget({Key? key, required this.image}) : super(key: key);

  @override
  _ItemBackgroundWidgetState createState() => _ItemBackgroundWidgetState();
}

class _ItemBackgroundWidgetState extends State<ItemBackgroundWidget> {
  late String _image;

  @override
  void initState() {
    _image = widget.image;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      child: Container(
          child: CachedNetworkImage(
            placeholder: (context, url) => loader(),
            imageUrl: _image,
              errorWidget: (context,url,error) => Icon(Icons.error_outline,size: 50,),fit: BoxFit.fill,filterQuality: FilterQuality.medium,
          ),
      ),
    );
  }
}
