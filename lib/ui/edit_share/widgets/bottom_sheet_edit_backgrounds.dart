import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marketing/ui/edit_share/model/offers_promos_model.dart';
import 'package:marketing/ui/edit_share/widgets/item_background.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:auto_size_text/auto_size_text.dart';

class BottomSheetEditBackgroundWidget extends StatefulWidget {
  final List<MultipleBackgroundImages> multipleBackgroundImages;

  const BottomSheetEditBackgroundWidget(
      {required this.multipleBackgroundImages})
      : super();

  @override
  _BottomSheetEditBackgroundWidgetState createState() =>
      _BottomSheetEditBackgroundWidgetState();
}

class _BottomSheetEditBackgroundWidgetState
    extends State<BottomSheetEditBackgroundWidget> {
  List<MultipleBackgroundImages> _multipleBackgroundImages = [];
  String mSelectedImage = "";

  @override
  void initState() {
    _multipleBackgroundImages = widget.multipleBackgroundImages;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        child: Container(
            height: ScreenSize.height(context) - 300,
            padding: EdgeInsets.only(
              bottom: ScreenSize.viewInsetsBottom(context),
            ),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(8, 10, 8, 10),
                width: ScreenSize.width(context),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        child: InkWell(
                          child: Image.asset(
                            "assets/images/header_bullet.png",
                            height: 25,
                            width: 10,
                          ),
                          onTap: () {},
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: AutoSizeText(
                          editBackground,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w600,
                              color: Color(0xff3D4B65)),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        child: InkWell(
                          customBorder: new CircleBorder(),
                          child: Icon(
                            Icons.close,
                            size: 30,
                            color: Colors.black,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                  child: Container(
                      margin: EdgeInsets.all(8),
                      child: _multipleBackgroundImages.length != 0
                          ? GridView.builder(
                              itemCount: _multipleBackgroundImages.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 8,
                                mainAxisSpacing: 8,
                                childAspectRatio: (1),
                              ),
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    setState(() {
                                      mSelectedImage =
                                          _multipleBackgroundImages[index]
                                              .image;
                                      Navigator.pop(context, mSelectedImage);
                                    });
                                  },
                                  child: ItemBackgroundWidget(
                                      image: _multipleBackgroundImages[index]
                                          .image),
                                );
                              })
                          : Center(
                              child: AutoSizeText(
                                noDataAvailable,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            )
                  )
              )
            ]
            )
        )
    );
  }
}
