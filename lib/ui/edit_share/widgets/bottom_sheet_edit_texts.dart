import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:auto_size_text/auto_size_text.dart';

class BottomSheetEditTextsWidgets extends StatefulWidget {
  const BottomSheetEditTextsWidgets({Key? key}) : super(key: key);

  @override
  _BottomSheetEditTextsWidgetsState createState() =>
      _BottomSheetEditTextsWidgetsState();
}

class _BottomSheetEditTextsWidgetsState
    extends State<BottomSheetEditTextsWidgets> {
  TextEditingController topLineController = new TextEditingController();
  TextEditingController middleLineController = new TextEditingController();
  TextEditingController bottomLineController = new TextEditingController();

  @override
  void initState() {
    topLineController.text = "";
    middleLineController.text = "";
    bottomLineController.text = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        child: Container(
            padding: EdgeInsets.only(
              bottom: ScreenSize.viewInsetsBottom(context),
            ),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(8, 10, 8, 10),
                width: ScreenSize.width(context),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        child: InkWell(
                          child: Image.asset(
                            "assets/images/header_bullet.png",
                            height: 25,
                            width: 10,
                          ),
                          onTap: () {},
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: AutoSizeText(
                          editText,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w600,
                              color: Color(0xff3D4B65)),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        child: InkWell(
                          customBorder: new CircleBorder(),
                          child: Icon(
                            Icons.close,
                            size: 30,
                            color: Colors.black,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: AutoSizeText(
                        topLine,
                        style: TextStyle(
                            color: Color(0xff3D4B65),
                            fontSize: 13,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffBBC3CF)),
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                        child: Row(
                          children: [
                            Expanded(
                                child: TextField(
                              controller: topLineController,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(8),
                                isDense: true,
                                hintText: "",
                                border: InputBorder.none,
                                labelStyle: TextStyle(
                                    fontSize: 15,
                                    color: Color(0xff3D4B65),
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w600),
                              ),
                            ))
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: AutoSizeText(
                        middleLine,
                        style: TextStyle(
                            color: Color(0xff3D4B65),
                            fontSize: 13,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffBBC3CF)),
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                  controller: middleLineController,
                                  textInputAction: TextInputAction.next,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    isDense: true,
                                    hintText: "",
                                    border: InputBorder.none,
                                    labelStyle: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xff3D4B65),
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w600),
                                  )),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: AutoSizeText(
                        bottomLine,
                        style: TextStyle(
                            color: Color(0xff3D4B65),
                            fontSize: 13,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffBBC3CF)),
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                  controller: bottomLineController,
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    isDense: true,
                                    hintText: "",
                                    border: InputBorder.none,
                                    labelStyle: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xff3D4B65),
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w600),
                                  )),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: ScreenSize.width(context),
                padding: EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: InkWell(
                  onTap: () {
                    print(
                        "===${topLineController.text}=====${middleLineController.text}=====${bottomLineController.text}");
                    HashMap map = HashMap<String, String>();
                    map["top"] = topLineController.text;
                    map["middle"] = middleLineController.text;
                    map["bottom"] = bottomLineController.text;
                    Navigator.pop(context, map);
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.fromLTRB(8, 5, 8, 5),
                    decoration: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Color(0xff1DC5E9), Color(0xff1EE1C3)],
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: AutoSizeText(
                        saveChanges,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              ),
            ])));
  }
}
