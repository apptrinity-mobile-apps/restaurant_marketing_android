import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/utils/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen() : super();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool? isUserLoggedIn = false;

  @override
  void initState() {
    SessionManager().isLoggedIn().then((value) {
      setState(() {
        isUserLoggedIn = value;
        isUserLoggedIn == true
            ? Navigator.of(context).pushReplacementNamed('/HomeScreen')
            : Navigator.of(context).pushReplacementNamed('/LoginScreen');
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff6f8fb),
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0xffF6F8FB),
            statusBarIconBrightness: Brightness.dark),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(8),
            color: Colors.white,
            child: FlutterLogo(size: ScreenSize.height(context))),
      ),
    );
  }
}
