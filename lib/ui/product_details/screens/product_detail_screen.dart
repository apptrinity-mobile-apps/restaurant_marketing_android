import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:marketing/ui/product_details/model/menu_model.dart';
import 'package:marketing/ui/product_details/viewModel/menu_view_model.dart';
import 'package:marketing/ui/product_details/widgets/bottom_sheet_change_product.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:marketing/utils/snack_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class ProductDetailScreen extends StatefulWidget {
  final String text;

  const ProductDetailScreen({Key? key, required this.text}) : super(key: key);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  ScreenshotController screenshotController = ScreenshotController();
  List<String> imagePaths = [];
  String mRestaurantName = "",
      mRestaurantId = "",
      mHeader = "",
      mEmployeeId = "",
      showing = "0",
      mCategoriesId = "";
  late MenuItems menuItem;
  bool isMenuItemSelected = false;

  void showBottomSheet() {
    final viewModel = Provider.of<MenuViewModel>(context, listen: false);
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        enableDrag: true,
        isDismissible: false,
        isScrollControlled: true,
        useRootNavigator: true,
        builder: (context) {
          return BottomSheetProductsWidget(vModel: viewModel);
        }).then((value) {
      var values = value == null ? MenuItems : value;
      setState(() {
        menuItem = values;
        isMenuItemSelected = true;
      });
    });
  }

  @override
  void initState() {
    if (widget.text == "new_launch") {
      mHeader = newLaunchesBestSellers;
      showing = "1";
    } else if (widget.text == "product_discount") {
      mHeader = productDiscount;
      showing = "0";
    }
    final vModel = Provider.of<MenuViewModel>(context, listen: false);
    SessionManager().getUserDetails().then((value) async {
      setState(() {
        mRestaurantName = value.employeeDetails.restaurantName;
        mRestaurantId = value.employeeDetails.restaurantId /*"6139b422d6a237714ed2abe2"*/;
        mEmployeeId = value.employeeDetails.id;
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      showSnackBar(context, pleaseWait);
      await vModel.getMenusApi(mRestaurantId).then((value) {
        Navigator.pop(context);
        if (value) {
          if (vModel.getMenus.isNotEmpty) {
            showBottomSheet();
          } else {
            showSnackBar(context, noDataAvailable);
          }
        } else {
          showSnackBar(context, tryAgain);
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<MenuViewModel>(context);
    return Scaffold(
      backgroundColor: Color(0xffF6F8FB),
      appBar: AppBar(
        leading: InkWell(
          customBorder: new CircleBorder(),
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back),
        ),
        backgroundColor: Color(0xff293855),
        title: Text(
          mHeader,
          style: TextStyle(
              fontSize: 18, fontFamily: "Poppins", fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        toolbarHeight: 56,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0x80101621),
            statusBarIconBrightness: Brightness.light),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          color: Colors.black12,
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: ScreenSize.height(context) / 1.85,
                  width: ScreenSize.width(context),
                  margin: EdgeInsets.all(10),
                  child: isMenuItemSelected
                      ? Screenshot(
                          controller: screenshotController,
                          child: Card(
                            elevation: 3,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            child: Container(
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      padding: EdgeInsets.all(10),
                                      margin:
                                          EdgeInsets.fromLTRB(10, 5, 10, 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 2),
                                            child: AutoSizeText(
                                              mRestaurantName,
                                              maxLines: 2,
                                              minFontSize: 14,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: Color(0xff3D4B65),
                                                  fontSize: 16,
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 2, 0, 0),
                                              child: RichText(
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                      text: "ORDER AT : ",
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xff64B5F6),
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 13)),
                                                  TextSpan(
                                                      text: viewModel
                                                          .restaurantDetails!
                                                          .onlineOrderUrl,
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xff64B5F6),
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 13)),
                                                ]),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                        child: Stack(
                                      fit: StackFit.expand,
                                      children: [
                                        Container(
                                          child: CachedNetworkImage(
                                            placeholder: (context, url) =>
                                                loader(),
                                            imageUrl: menuItem.itemImage,
                                            errorWidget:
                                                (context, url, error) => Icon(
                                              Icons.error_outline,
                                              size: 70,
                                            ),
                                            fit: BoxFit.fill,
                                            filterQuality: FilterQuality.medium,
                                          ),
                                        ),
                                        showing == "1"
                                            ? Positioned(
                                                top: 0,
                                                left: 0,
                                                right: 0,
                                                bottom: 100,
                                                child: FittedBox(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    fit: BoxFit.none,
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(25),
                                                              bottomLeft: Radius
                                                                  .circular(
                                                                      25)),
                                                      child: Container(
                                                        color:
                                                            Color(0xB6FF7266),
                                                        padding:
                                                            EdgeInsets.all(8),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.all(8),
                                                          child: AutoSizeText(
                                                            bestSeller,
                                                            maxLines: 2,
                                                            minFontSize: 14,
                                                            textAlign:
                                                                TextAlign.end,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 16,
                                                                fontFamily:
                                                                    "Poppins",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                        ),
                                                      ),
                                                    )))
                                            : SizedBox(),
                                      ],
                                    )),
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      margin:
                                          EdgeInsets.fromLTRB(10, 5, 10, 10),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: AutoSizeText(
                                              menuItem.name,
                                              maxLines: 2,
                                              minFontSize: 14,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: Color(0xff3D4B65),
                                                  fontSize: 16,
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            child: AutoSizeText(
                                              "\u0024${menuItem.basePrice}",
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: Color(0xff64B5F6),
                                                  fontSize: 16,
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      : Center(
                          child: AutoSizeText(
                            "Please select any product!",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                        ),
                )
              ]),
        ),
      ),
      bottomSheet: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            children: [
              Container(
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                child: Material(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            showBottomSheet();
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.all(0),
                                child: Image.asset(
                                  "assets/images/change_product_rounded.png",
                                  height: 60,
                                  width: 60,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                child: AutoSizeText(
                                  changeProduct,
                                  style: TextStyle(
                                      color: Color(0xff3D4B65),
                                      fontSize: 13,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w600),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            systemShare();
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.all(0),
                                child: Image.asset(
                                  "assets/images/share_product_rounded.png",
                                  height: 60,
                                  width: 60,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                child: AutoSizeText(
                                  share,
                                  style: TextStyle(
                                      color: Color(0xff3D4B65),
                                      fontSize: 13,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w600),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            whatsappShare();
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.all(0),
                                child: Image.asset(
                                  "assets/images/share_whatsapp_black_rounded.png",
                                  height: 60,
                                  width: 60,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                child: AutoSizeText(
                                  whatsApp,
                                  style: TextStyle(
                                      color: Color(0xff3D4B65),
                                      fontSize: 13,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w600),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  systemShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
            pixelRatio: ScreenSize.pixelRatio(context),
            delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      setState(() {
        imagePaths.clear();
        imagePaths.add(imgFile.path);
      });
      final RenderBox box = context.findRenderObject() as RenderBox;
      Share.shareFiles(imagePaths,
          text: "Sharing image",
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });
  }

  whatsappShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
            pixelRatio: ScreenSize.pixelRatio(context),
            delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      FlutterShareMe()
          .shareToWhatsApp(
          msg: "Sharing image",
          imagePath: imgFile.path,
          fileType: FileType.image)
          .then((value) {
        if (value!.toString().contains("PlatformException")) {
          showSnackBar(context, unableToShareWhatsApp);
        }
      });
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });
  }
}
