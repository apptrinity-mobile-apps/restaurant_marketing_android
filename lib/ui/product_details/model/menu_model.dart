class MenusModelResponse {
  late List<Menus> menus;
  late int responseStatus;
  late RestaurantDetails restaurantDetails;
  late String result;

  MenusModelResponse(
      {required this.menus,
      required this.responseStatus,
      required this.restaurantDetails,
      required this.result});

  MenusModelResponse.fromJson(Map<String, dynamic> json) {
    if (json['menus'] != null) {
      menus = <Menus>[];
      json['menus'].forEach((v) {
        menus.add(new Menus.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    restaurantDetails = (json['restaurantDetails'] != null
        ? new RestaurantDetails.fromJson(json['restaurantDetails'])
        : null)!;
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menus'] = this.menus.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['restaurantDetails'] = this.restaurantDetails.toJson();
    data['result'] = this.result;
    return data;
  }
}

class Menus {
  late String id;
  late List<MenuGroups> menuGroups;
  late String menuName;
  late String restaurantId;

  Menus(
      {required this.id,
      required this.menuGroups,
      required this.menuName,
      required this.restaurantId});

  Menus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['menuGroups'] != null) {
      menuGroups = <MenuGroups>[];
      json['menuGroups'].forEach((v) {
        menuGroups.add(new MenuGroups.fromJson(v));
      });
    }
    menuName = json['menuName'];
    restaurantId = json['restaurantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['menuGroups'] = this.menuGroups.map((v) => v.toJson()).toList();
    data['menuName'] = this.menuName;
    data['restaurantId'] = this.restaurantId;
    return data;
  }
}

class MenuGroups {
  late String groupDescription;
  late String id;
  late List<MenuItems> menuItems;
  late String name;
  late int priceStrategy;
  late String restaurantId;
  late String type;

  MenuGroups(
      {required this.groupDescription,
      required this.id,
      required this.menuItems,
      required this.name,
      required this.priceStrategy,
      required this.restaurantId,
      required this.type});

  MenuGroups.fromJson(Map<String, dynamic> json) {
    groupDescription =
        json['groupDescription'] == null ? "" : json['groupDescription'];
    id = json['id'] == null ? "" : json['id'];
    if (json['menuItems'] != null) {
      menuItems = <MenuItems>[];
      json['menuItems'].forEach((v) {
        menuItems.add(new MenuItems.fromJson(v));
      });
    }
    name = json['name'] == null ? "" : json['name'];
    priceStrategy = json['priceStrategy'] == null ? 0 : json['priceStrategy'];
    restaurantId = json['restaurantId'] == null ? "" : json['restaurantId'];
    type = json['type'] == null ? "" : json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['groupDescription'] = this.groupDescription;
    data['id'] = this.id;
    data['menuItems'] = this.menuItems.map((v) => v.toJson()).toList();
    data['name'] = this.name;
    data['priceStrategy'] = this.priceStrategy;
    data['restaurantId'] = this.restaurantId;
    data['type'] = this.type;
    return data;
  }
}

class MenuItems {
  late String basePrice;
  late bool diningOptionTaxException;
  late bool diningTaxOption;
  late String id;
  late bool inheritDiningOptionTax;
  late bool inheritTaxInclude;
  late bool inheritTaxRate;
  late int inventory;
  late String itemDescription;
  late String itemImage;
  late int maxQuantity;
  late String name;
  late int pricingStrategy;
  late String restaurantId;
  late bool taxIncludeOption;
  late List<TaxesList> taxesList;
  late String type;
  late List<SizeList> sizeList;
  late List<TimePriceList> timePriceList;

  MenuItems(
      {required this.basePrice,
      required this.diningOptionTaxException,
      required this.diningTaxOption,
      required this.id,
      required this.inheritDiningOptionTax,
      required this.inheritTaxInclude,
      required this.inheritTaxRate,
      required this.inventory,
      required this.itemDescription,
      required this.itemImage,
      required this.maxQuantity,
      required this.name,
      required this.pricingStrategy,
      required this.restaurantId,
      required this.taxIncludeOption,
      required this.taxesList,
      required this.type,
      required this.sizeList,
      required this.timePriceList});

  MenuItems.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'] == null ? "" : json['basePrice'];
    diningOptionTaxException = json['diningOptionTaxException'] == null
        ? false
        : json['diningOptionTaxException'];
    diningTaxOption =
        json['diningTaxOption'] == null ? false : json['diningTaxOption'];
    id = json['id'] == null ? "" : json['id'];
    inheritDiningOptionTax = json['inheritDiningOptionTax'] == null
        ? false
        : json['inheritDiningOptionTax'];
    inheritTaxInclude =
        json['inheritTaxInclude'] == null ? false : json['inheritTaxInclude'];
    inheritTaxRate =
        json['inheritTaxRate'] == null ? false : json['inheritTaxRate'];
    inventory = json['inventory'] == null ? 0 : json['inventory'];
    itemDescription =
        json['itemDescription'] == null ? "" : json['itemDescription'];
    itemImage = json['itemImage'] == null ? "" : json['itemImage'];
    maxQuantity = json['maxQuantity'] == null ? 0 : json['maxQuantity'];
    name = json['name'] == null ? "" : json['name'];
    pricingStrategy =
        json['pricingStrategy'] == null ? 0 : json['pricingStrategy'];
    restaurantId = json['restaurantId'] == null ? "" : json['restaurantId'];
    taxIncludeOption =
        json['taxIncludeOption'] == null ? false : json['taxIncludeOption'];
    if (json['taxesList'] != null) {
      taxesList = <TaxesList>[];
      json['taxesList'].forEach((v) {
        taxesList.add(new TaxesList.fromJson(v));
      });
    }
    type = json['type'] == null ? "" : json['type'];
    if (json['sizeList'] != null) {
      sizeList = <SizeList>[];
      json['sizeList'].forEach((v) {
        sizeList.add(new SizeList.fromJson(v));
      });
    }
    if (json['timePriceList'] != null) {
      timePriceList = <TimePriceList>[];
      json['timePriceList'].forEach((v) {
        timePriceList.add(new TimePriceList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['diningOptionTaxException'] = this.diningOptionTaxException;
    data['diningTaxOption'] = this.diningTaxOption;
    data['id'] = this.id;
    data['inheritDiningOptionTax'] = this.inheritDiningOptionTax;
    data['inheritTaxInclude'] = this.inheritTaxInclude;
    data['inheritTaxRate'] = this.inheritTaxRate;
    data['inventory'] = this.inventory;
    data['itemDescription'] = this.itemDescription;
    data['itemImage'] = this.itemImage;
    data['maxQuantity'] = this.maxQuantity;
    data['name'] = this.name;
    data['pricingStrategy'] = this.pricingStrategy;
    data['restaurantId'] = this.restaurantId;
    data['taxIncludeOption'] = this.taxIncludeOption;
    data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    data['type'] = this.type;
    data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    data['timePriceList'] = this.timePriceList.map((v) => v.toJson()).toList();
    return data;
  }
}

class TaxesList {
  late bool mDefault;
  late String importId;
  late int roundingOptions;
  late int status;
  late String taxName;
  late String taxRate;
  late List<TaxTable> taxTable;
  late int taxType;
  late String taxid;
  late int uniqueNumber;

  TaxesList(
      {required this.mDefault,
      required this.importId,
      required this.roundingOptions,
      required this.status,
      required this.taxName,
      required this.taxRate,
      required this.taxTable,
      required this.taxType,
      required this.taxid,
      required this.uniqueNumber});

  TaxesList.fromJson(Map<String, dynamic> json) {
    mDefault = json['default'] == null ? false : json['default'];
    importId = json['importId'] == null ? "" : json['importId'];
    roundingOptions =
        json['roundingOptions'] == null ? 0 : json['roundingOptions'];
    status = json['status'] == null ? 0 : json['status'];
    taxName = json['taxName'] == null ? "" : json['taxName'];
    taxRate = json['taxRate'] == null ? "" : json['taxRate'];
    if (json['taxTable'] != null) {
      taxTable = <TaxTable>[];
      json['taxTable'].forEach((v) {
        taxTable.add(new TaxTable.fromJson(v));
      });
    }
    taxType = json['taxType'] == null ? 0 : json['taxType'];
    taxid = json['taxid'] == null ? "" : json['taxid'];
    uniqueNumber = json['uniqueNumber'] == null ? 0 : json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['default'] = this.mDefault;
    data['importId'] = this.importId;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    data['taxType'] = this.taxType;
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

class TaxTable {
  late double from;
  late double priceDifference;
  late bool repeat;
  late double taxApplied;
  late int to;

  TaxTable(
      {required this.from,
      required this.priceDifference,
      required this.repeat,
      required this.taxApplied,
      required this.to});

  TaxTable.fromJson(Map<String, dynamic> json) {
    from = json['from'] == null ? 0.0 : double.parse(json['from'].toString());
    priceDifference = json['priceDifference'] == null
        ? 0.0
        : double.parse(json['priceDifference'].toString());
    repeat = json['repeat'] == null ? false : json['repeat'];
    taxApplied = json['taxApplied'] == null
        ? 0.0
        : double.parse(json['taxApplied'].toString());
    to = json['to'] == null ? 0 : int.parse(json['to'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from'] = this.from;
    data['priceDifference'] = this.priceDifference;
    data['repeat'] = this.repeat;
    data['taxApplied'] = this.taxApplied;
    data['to'] = this.to;
    return data;
  }
}

class SizeList {
  late double price;
  late String sizeName;

  SizeList({required this.price, required this.sizeName});

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'] == null ? 0.0 : json['price'];
    sizeName = json['sizeName'] == null ? "" : json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

class TimePriceList {
  late List<String> days;
  late double price;
  late String timeFrom;
  late String timeTo;

  TimePriceList(
      {required this.days,
      required this.price,
      required this.timeFrom,
      required this.timeTo});

  TimePriceList.fromJson(Map<String, dynamic> json) {
    days = json['days'].cast<String>();
    price = json['price'] == null ? "" : json['price'];
    timeFrom = json['timeFrom'] == null ? 0.0 : json['timeFrom'];
    timeTo = json['timeTo'] == null ? "" : json['timeTo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['days'] = this.days;
    data['price'] = this.price;
    data['timeFrom'] = this.timeFrom;
    data['timeTo'] = this.timeTo;
    return data;
  }
}

class RestaurantDetails {
  late String cityId;
  late String countryId;
  late String createdOn;
  late String email;
  late String franchiseId;
  late String id;
  late bool isFranchiseRestaurant;
  late String leadId;
  late String location;
  late String resType;
  late int restauranPhNo;
  late String restaurantDescription;
  late String restaurantId;
  late String restaurantLogo;
  late String restaurantName;
  late String restaurantType;
  late String stateId;
  late String onlineOrderUrl;
  late int status;

  RestaurantDetails(
      {required this.cityId,
      required this.countryId,
      required this.createdOn,
      required this.email,
      required this.franchiseId,
      required this.id,
      required this.isFranchiseRestaurant,
      required this.leadId,
      required this.location,
      required this.resType,
      required this.restauranPhNo,
      required this.restaurantDescription,
      required this.restaurantId,
      required this.restaurantLogo,
      required this.restaurantName,
      required this.restaurantType,
      required this.stateId,
      required this.onlineOrderUrl,
      required this.status});

  RestaurantDetails.fromJson(Map<String, dynamic> json) {
    cityId = json['cityId'] == null ? "" : json['cityId'];
    countryId = json['countryId'] == null ? "" : json['countryId'];
    createdOn = json['createdOn'] == null ? "" : json['createdOn'];
    email = json['email'] == null ? "" : json['email'];
    franchiseId = json['franchiseId'] == null ? "" : json['franchiseId'];
    id = json['id'] == null ? "" : json['id'];
    isFranchiseRestaurant = json['isFranchiseRestaurant'] == null
        ? false
        : json['isFranchiseRestaurant'];
    leadId = json['leadId'] == null ? "" : json['leadId'];
    location = json['location'] == null ? "" : json['location'];
    resType = json['resType'] == null ? "" : json['resType'];
    restauranPhNo = json['restauranPhNo'] == null ? 0 : json['restauranPhNo'];
    restaurantDescription = json['restaurantDescription'] == null
        ? ""
        : json['restaurantDescription'];
    restaurantId = json['restaurantId'] == null ? "" : json['restaurantId'];
    restaurantLogo =
        json['restaurantLogo'] == null ? "" : json['restaurantLogo'];
    restaurantName =
        json['restaurantName'] == null ? "" : json['restaurantName'];
    restaurantType =
        json['restaurantType'] == null ? "" : json['restaurantType'];
    onlineOrderUrl =
        json['onlineOrderUrl'] == null ? "" : json['onlineOrderUrl'];
    stateId = json['stateId'] == null ? "" : json['stateId'];
    status = json['status'] == null ? 0 : json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cityId'] = this.cityId;
    data['countryId'] = this.countryId;
    data['createdOn'] = this.createdOn;
    data['email'] = this.email;
    data['franchiseId'] = this.franchiseId;
    data['id'] = this.id;
    data['isFranchiseRestaurant'] = this.isFranchiseRestaurant;
    data['leadId'] = this.leadId;
    data['location'] = this.location;
    data['resType'] = this.resType;
    data['restauranPhNo'] = this.restauranPhNo;
    data['restaurantDescription'] = this.restaurantDescription;
    data['restaurantId'] = this.restaurantId;
    data['restaurantLogo'] = this.restaurantLogo;
    data['restaurantName'] = this.restaurantName;
    data['restaurantType'] = this.restaurantType;
    data['stateId'] = this.stateId;
    data['onlineOrderUrl'] = this.onlineOrderUrl;
    data['status'] = this.status;
    return data;
  }
}
