import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/ui/product_details/model/menu_model.dart';
import 'package:marketing/ui/product_details/viewModel/menu_view_model.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:group_list_view/group_list_view.dart';

class BottomSheetProductsWidget extends StatefulWidget {
  final MenuViewModel vModel;

  const BottomSheetProductsWidget({required this.vModel}) : super();

  @override
  _BottomSheetProductsWidgetState createState() =>
      _BottomSheetProductsWidgetState();
}

class _BottomSheetProductsWidgetState extends State<BottomSheetProductsWidget> {
  TextEditingController searchController = new TextEditingController();
  List<MenuGroups> mMenuGroupsList = [];
  List<MenuGroups> mSearchList = [];
  late MenuViewModel viewModel;
  bool dataLoaded = false;

  @override
  void initState() {
    viewModel = widget.vModel;
    super.initState();
  }

  Widget menuListView(MenuViewModel viewModel) {
    bool isFetching = viewModel.isFetching ? true : false;
    bool hasData = viewModel.isHavingMenus ? true : false;
    bool havingData = viewModel.getMenus.length > 0 ? true : false;
    if (!dataLoaded) {
      setState(() {
        mMenuGroupsList = [];
        mSearchList = [];
      });
      for (int i = 0; i < viewModel.getMenus.length; i++) {
        mMenuGroupsList.addAll(viewModel.getMenus[i].menuGroups);
      }
      setState(() {
        mSearchList = mMenuGroupsList;
        dataLoaded = true;
      });
    }

    return isFetching
        ? Container(
            child: loader(),
          )
        : hasData
            ? (havingData
                ? GroupListView(
                    sectionsCount: mSearchList.length,
                    countOfItemInSection: (int section) {
                      return mSearchList[section].menuItems.length;
                    },
                    groupHeaderBuilder: (BuildContext context, int section) {
                      return mSearchList[section].menuItems.length > 0
                          ? Container(
                              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                              margin: EdgeInsets.fromLTRB(8, 10, 8, 10),
                              width: ScreenSize.width(context),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      child: Image.asset(
                                        "assets/images/header_bullet.png",
                                        height: 25,
                                        width: 10,
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                      child: AutoSizeText(
                                        mSearchList[section].name,
                                        textAlign: TextAlign.center,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xff3D4B65)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : SizedBox();
                    },
                    itemBuilder: (BuildContext context, IndexPath index) {
                      return mSearchList[index.section].menuItems.length > 0
                          ? InkWell(
                              onTap: () {
                                Navigator.pop(
                                    context,
                                    mSearchList[index.section]
                                        .menuItems[index.index]);
                              },
                              child: Expanded(
                                  child: Container(
                                      width: ScreenSize.width(context),
                                      height: ScreenSize.height(context) / 6,
                                      margin: EdgeInsets.fromLTRB(8, 0, 8, 4),
                                      child: Card(
                                          elevation: 3,
                                          color: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                  child: Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    8, 8, 0, 8),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: CachedNetworkImage(
                                                    placeholder:
                                                        (context, url) =>
                                                            loader(),
                                                    imageUrl: mSearchList[
                                                            index.section]
                                                        .menuItems[index.index]
                                                        .itemImage,
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(
                                                      Icons.error_outline,
                                                      size: 50,
                                                    ),
                                                    fit: BoxFit.fill,
                                                    filterQuality:
                                                        FilterQuality.medium,
                                                  ),
                                                ),
                                              )),
                                              Expanded(
                                                child: Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    margin: EdgeInsets.fromLTRB(
                                                        0, 8, 8, 8),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Padding(
                                                          padding: EdgeInsets
                                                              .fromLTRB(
                                                                  8, 8, 8, 4),
                                                          child: AutoSizeText(
                                                            mSearchList[index
                                                                    .section]
                                                                .menuItems[
                                                                    index.index]
                                                                .name,
                                                            maxLines: 2,
                                                            minFontSize: 14,
                                                            maxFontSize: 16,
                                                            textAlign:
                                                                TextAlign.start,
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xff3D4B65),
                                                                fontSize: 16,
                                                                fontFamily:
                                                                    "Poppins",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets
                                                              .fromLTRB(
                                                                  8, 4, 8, 8),
                                                          child: AutoSizeText(
                                                            "\u0024${mSearchList[index.section].menuItems[index.index].basePrice}",
                                                            textAlign:
                                                                TextAlign.start,
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xff64B5F6),
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    "Poppins",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                        ),
                                                      ],
                                                    )),
                                                flex: 2,
                                              ),
                                            ],
                                          )))))
                          : SizedBox();
                    },
                  )
                : Center(
                    child: AutoSizeText(
                      noDataAvailable,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ))
            : Center(
                child: AutoSizeText(
                  noDataAvailable,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              );
  }

  // This function is called whenever the text field changes
  void _runFilter(String enteredKeyword) {
    List<MenuGroups> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all menus
      results = mMenuGroupsList;
      // Refresh the UI
      setState(() {
        mSearchList = [];
        mSearchList = results;
        mSearchList.forEach((element) {
          print("----${element.name}");
        });
      });
    } else {
      late List<MenuGroups> list;
      late List<MenuItems> itemsList;
      setState(() {
        list = mMenuGroupsList;
        list.forEach((menuGroup) {
          setState(() {
            itemsList = menuGroup.menuItems;
          });
          // we use the toLowerCase() method to make it case-insensitive
          itemsList.forEach((menuItem) {
            if (menuItem.name
                .toLowerCase()
                .contains(enteredKeyword.toLowerCase())) {
              results.add(menuGroup);
            }
          });
        });
      });
      // Refresh the UI
      setState(() {
        mSearchList = [];
        mSearchList = results;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Container(
          height: ScreenSize.height(context) - 100,
          padding: EdgeInsets.only(
            bottom: ScreenSize.viewInsetsBottom(context),
          ),
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              margin: EdgeInsets.fromLTRB(8, 10, 8, 10),
              width: ScreenSize.width(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      child: InkWell(
                        child: Image.asset(
                          "assets/images/header_bullet.png",
                          height: 25,
                          width: 10,
                        ),
                        onTap: () {},
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: AutoSizeText(
                        availableItems,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w600,
                            color: Color(0xff3D4B65)),
                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      child: InkWell(
                        customBorder: new CircleBorder(),
                        child: Icon(
                          Icons.close,
                          size: 30,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(8, 0, 8, 5),
                width: ScreenSize.width(context),
                child: RichText(
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: warningProduct1,
                        style: TextStyle(
                            color: Color(0xff64B5F6),
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            fontSize: 14)),
                    TextSpan(
                        text: warningProduct2,
                        style: TextStyle(
                            color: Color(0xff3D4B65),
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            fontSize: 14)),
                  ]),
                )),
            Container(
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xffBBC3CF)),
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Icon(
                        Icons.search_rounded,
                        size: 24,
                      ),
                    ),
                    Expanded(
                        child: TextField(
                      onChanged: (value) => _runFilter(value),
                      controller: searchController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(8),
                          isDense: true,
                          hintText: searchProducts,
                          border: InputBorder.none,
                          labelStyle: TextStyle(
                              fontSize: 15,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w400),
                          hintStyle: TextStyle(
                              fontSize: 15,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w400)),
                    )),
                    searchController.text.isNotEmpty
                        ? Material(
                            child: InkWell(
                              customBorder: new CircleBorder(),
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                searchController.clear();
                                _runFilter("");
                              },
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Icon(
                                  Icons.close,
                                  size: 24,
                                ),
                              ),
                            ),
                          )
                        : SizedBox()
                  ],
                ),
              ),
            ),
            Expanded(
              child: menuListView(viewModel),
              flex: 1,
            )
          ])),
    );
  }
}
