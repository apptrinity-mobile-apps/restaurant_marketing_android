import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:marketing/services/repositories.dart';
import 'package:marketing/ui/product_details/model/menu_model.dart';

class MenuViewModel with ChangeNotifier {
  bool _isFetching = false;
  bool _isHavingMenus = false;
  List<Menus> menus = [];
  RestaurantDetails? _restaurantDetails;

  bool get isFetching => _isFetching;

  bool get isHavingMenus => _isHavingMenus;

  List<Menus> get getMenus {
    return menus;
  }

  RestaurantDetails? get restaurantDetails {
    return _restaurantDetails;
  }

  Future<bool> getMenusApi(String restaurantId) async {
    _isFetching = true;
    _isHavingMenus = false;
    _restaurantDetails = null;
    notifyListeners();
    menus.clear();
    try {
      dynamic response = await Repository().menu(restaurantId);
      if (response != null) {
        MenusModelResponse _menusModelResponse = MenusModelResponse.fromJson(response);
        _restaurantDetails = _menusModelResponse.restaurantDetails;
        menus.addAll(_menusModelResponse.menus);
        _isHavingMenus = true;
      } else {
        _restaurantDetails= null;
        _isHavingMenus = false;
      }
    } catch (e) {
      print("error $e");
      _restaurantDetails= null;
      _isHavingMenus = false;
    }
    _isFetching = false;
    notifyListeners();
    return _isHavingMenus;
  }
}
