class SocialMediaCategoriesResponse {
  late int responseStatus;
  late String result;
  late List<SocialMediaCategoriesList> socialMediaCategoriesList;

  SocialMediaCategoriesResponse(
      {required this.responseStatus,
      required this.result,
      required this.socialMediaCategoriesList});

  SocialMediaCategoriesResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['socialMediaCategoriesList'] != null) {
      socialMediaCategoriesList = <SocialMediaCategoriesList>[];
      json['socialMediaCategoriesList'].forEach((v) {
        socialMediaCategoriesList
            .add(new SocialMediaCategoriesList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    data['socialMediaCategoriesList'] =
        this.socialMediaCategoriesList.map((v) => v.toJson()).toList();
    return data;
  }
}

class SocialMediaCategoriesList {
  late String categoryName;
  late String createdOn;
  late String id;
  late String image;
  late int status;
  late String userId;

  SocialMediaCategoriesList(
      {required this.categoryName,
      required this.createdOn,
      required this.id,
      required this.image,
      required this.status,
      required this.userId});

  SocialMediaCategoriesList.fromJson(Map<String, dynamic> json) {
    categoryName = json['categoryName'] == null ? "" :json['categoryName'];
    createdOn = json['createdOn'] == null ? "" :json['createdOn'];
    id = json['id'] == null ? "" :json['id'];
    image = json['image'] == null ? "" :json['image'];
    status = json['status'] == null ? 0 :json['status'];
    userId = json['userId'] == null ? "" :json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categoryName'] = this.categoryName;
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['image'] = this.image;
    data['status'] = this.status;
    data['userId'] = this.userId;
    return data;
  }
}
