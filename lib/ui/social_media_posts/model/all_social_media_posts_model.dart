class SocialMediaPostsResponse {
  late List<SocialMediaPostsList> socialMediaPostsList;
  late int responseStatus;
  late String result;

  SocialMediaPostsResponse(
      {required this.socialMediaPostsList,
      required this.responseStatus,
      required this.result});

  SocialMediaPostsResponse.fromJson(Map<String, dynamic> json) {
    if (json['socialMediaPostsList'] != null) {
      socialMediaPostsList = <SocialMediaPostsList>[];
      json['socialMediaPostsList'].forEach((v) {
        socialMediaPostsList.add(new SocialMediaPostsList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['socialMediaPostsList'] =
        this.socialMediaPostsList.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class SocialMediaPostsList {
  late String categoryName;
  late String createdOn;
  late String image;
  late bool isFavourite;
  late String socialMediaCategoryId;
  late String socialMediaPostsId;
  late int status;

  SocialMediaPostsList(
      {required this.categoryName,
      required this.createdOn,
      required this.image,
      required this.isFavourite,
      required this.socialMediaCategoryId,
      required this.socialMediaPostsId,
      required this.status});

  SocialMediaPostsList.fromJson(Map<String, dynamic> json) {
    categoryName = json['categoryName'] == null ? "" :json['categoryName'];
    createdOn = json['createdOn'] == null ? "" :json['createdOn'];
    image = json['image'] == null ? "" :json['image'];
    isFavourite = json['isFavourite'] == null ? false :json['isFavourite'];
    socialMediaCategoryId = json['socialMediaCategoryId'] == null ? "" :json['socialMediaCategoryId'];
    socialMediaPostsId = json['socialMediaPostsId'] == null ? "" :json['socialMediaPostsId'];
    status = json['status'] == null ? 0 :json['status'] ;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categoryName'] = this.categoryName;
    data['createdOn'] = this.createdOn;
    data['image'] = this.image;
    data['isFavourite'] = this.isFavourite;
    data['socialMediaCategoryId'] = this.socialMediaCategoryId;
    data['socialMediaPostsId'] = this.socialMediaPostsId;
    data['status'] = this.status;
    return data;
  }
}
