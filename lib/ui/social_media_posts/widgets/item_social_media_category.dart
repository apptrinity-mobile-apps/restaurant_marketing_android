import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marketing/ui/social_media_posts/model/social_media_categories_response_model.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';

class SocialMediaCategoryWidget extends StatefulWidget {
  final SocialMediaCategoriesList categoriesListItem;
  final int selectedIndex;
  final int index;

  const SocialMediaCategoryWidget(
      {Key? key,
      required this.categoriesListItem,
      required this.selectedIndex,
      required this.index})
      : super(key: key);

  @override
  _SocialMediaCategoryWidgetState createState() =>
      _SocialMediaCategoryWidgetState();
}

class _SocialMediaCategoryWidgetState extends State<SocialMediaCategoryWidget> {
  late SocialMediaCategoriesList item;
  String mCategoryId = "";

  @override
  void initState() {
    setState(() {
      item = widget.categoriesListItem;
      mCategoryId = item.id;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: ScreenSize.width(context) / 3.25,
        height: ScreenSize.height(context) ,
        margin: EdgeInsets.fromLTRB(4, 8, 4, 4),
        padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: widget.selectedIndex == widget.index
              ? Colors.black12
              : Colors.transparent,
        ),
        child: Column(
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.fromLTRB(4, 5, 4, 0),
                child: Card(
                  elevation: 3,
                  clipBehavior: Clip.antiAlias,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: item.categoryName == favorites
                      ? Image.asset("assets/images/fav_default.png",
                          fit: BoxFit.fill)
                      : CachedNetworkImage(
                      placeholder: (context, url) => loader(),
                      imageUrl: item.image,
                      errorWidget: (context,url,error) => Icon(Icons.error_outline,size:ScreenSize.width(context) / 4.5,)
                    ,fit: BoxFit.fill,filterQuality: FilterQuality.medium,),
                ),
              ),
              flex: 3,
            ),
            Flexible(
                child: Padding(
              padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
              child: AutoSizeText(
                item.categoryName,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                maxLines: 2,
                minFontSize: 12,
                maxFontSize: 14,
                style: TextStyle(
                    color: Color(0xff3D4B65),
                    fontSize: 14,
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.w500),
              ),
            ))
          ],
        ));
  }
}
