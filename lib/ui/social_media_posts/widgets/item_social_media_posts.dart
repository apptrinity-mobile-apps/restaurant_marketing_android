import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marketing/ui/social_media_posts/model/all_social_media_posts_model.dart';
import 'package:marketing/ui/social_media_posts/viewModel/social_media_view_model.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:marketing/utils/snack_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';
import 'package:like_button/like_button.dart';
import 'package:flutter_share_me/flutter_share_me.dart';

class SocialMediaPostsWidget extends StatefulWidget {
  final postsListItem;
  final selectedCatId;

  const SocialMediaPostsWidget(
      {Key? key, required this.postsListItem, required this.selectedCatId})
      : super(key: key);

  @override
  _SocialMediaPostsWidgetState createState() => _SocialMediaPostsWidgetState();
}

class _SocialMediaPostsWidgetState extends State<SocialMediaPostsWidget>
    with TickerProviderStateMixin {
  late bool isLiked;
  late SocialMediaPostsList item;
  String mRestaurantId = "", mEmployeeId = "", mCategoriesId = "";
  List<String> imagePaths = [];
  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mCategoriesId = widget.selectedCatId;
        mRestaurantId = value.employeeDetails.restaurantId;
        mEmployeeId = value.employeeDetails.id;
      });
    });
    isLiked = false;
    setState(() {
      item = widget.postsListItem;
      isLiked = item.isFavourite;
    });
    super.initState();
  }

  setFavourite(SocialMediaCategoriesViewModel viewModel) async {
    if (!isLiked) {
      await viewModel
          .favouritePost(item.socialMediaPostsId, mEmployeeId, mRestaurantId)
          .then((value) {
        setState(() {
          isLiked = value;
        });
      });
    } else {
      await viewModel
          .unFavouritePost(item.socialMediaPostsId, mEmployeeId, mRestaurantId)
          .then((value) {
        setState(() {
          isLiked = !value;
        });
        if (mCategoriesId == "0") {
          viewModel.getSocialMediaPosts(
              mCategoriesId, mEmployeeId, mRestaurantId);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final viewModel =
        Provider.of<SocialMediaCategoriesViewModel>(context, listen: true);
    return Container(
        width: ScreenSize.width(context) / 1,
        height: ScreenSize.height(context) / 1.95,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
        child: Screenshot(
            controller: screenshotController,
            child: Card(
              elevation: 3,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: CachedNetworkImage(
                            placeholder: (context, url) => loader(),
                            imageUrl: item.image,
                            errorWidget: (context, url, error) => Icon(
                              Icons.error_outline,
                              size: 50,
                            ),
                            fit: BoxFit.fill,
                            filterQuality: FilterQuality.medium,
                          ),
                        )),
                    flex: 2,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: LikeButton(
                                  size: 35,
                                  isLiked: isLiked,
                                  likeBuilder: (isLiked) {
                                    final color = isLiked
                                        ? favourite_color
                                        : un_favourite_color;
                                    return Icon(
                                      Icons.favorite,
                                      color: color,
                                      size: 35,
                                    );
                                  },
                                  circleColor: CircleColor(
                                      start: Colors.red, end: Colors.redAccent),
                                  bubblesColor: BubblesColor(
                                      dotPrimaryColor: Colors.red,
                                      dotSecondaryColor: Colors.pinkAccent,
                                      dotThirdColor: Colors.orangeAccent,
                                      dotLastColor: Colors.yellowAccent),
                                  bubblesSize: 70,
                                  onTap: (isLiked) async {
                                    setFavourite(viewModel);
                                    return !isLiked;
                                  },
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: AutoSizeText(
                                  mFav,
                                  style: TextStyle(
                                      color: Color(0xff3D4B65),
                                      fontSize: 13,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              InkWell(
                                customBorder: new CircleBorder(),
                                onTap: () {
                                  systemShare();
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(5),
                                  child: Image.asset(
                                    "assets/images/share.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: AutoSizeText(
                                  share,
                                  style: TextStyle(
                                      color: Color(0xff3D4B65),
                                      fontSize: 13,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              InkWell(
                                customBorder: new CircleBorder(),
                                onTap: () {
                                  whatsappShare();
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(5),
                                  child: Image.asset(
                                    "assets/images/whatsapp.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: AutoSizeText(
                                  whatsApp,
                                  style: TextStyle(
                                      color: Color(0xff3D4B65),
                                      fontSize: 13,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )));
  }

  systemShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
            pixelRatio: ScreenSize.pixelRatio(context),
            delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      setState(() {
        imagePaths.clear();
        imagePaths.add(imgFile.path);
      });
      final RenderBox box = context.findRenderObject() as RenderBox;
      Share.shareFiles(imagePaths,
          text: "Sharing image",
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });
  }

  whatsappShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
            pixelRatio: ScreenSize.pixelRatio(context),
            delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      FlutterShareMe()
          .shareToWhatsApp(
          msg: "Sharing image",
          imagePath: imgFile.path,
          fileType: FileType.image)
          .then((value) {
        if (value!.toString().contains("PlatformException")) {
          showSnackBar(context, unableToShareWhatsApp);
        }
      });
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });

  }
}
