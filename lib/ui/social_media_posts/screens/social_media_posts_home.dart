import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:marketing/ui/social_media_posts/viewModel/social_media_view_model.dart';
import 'package:marketing/ui/social_media_posts/widgets/item_social_media_category.dart';
import 'package:marketing/ui/social_media_posts/widgets/item_social_media_posts.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:provider/provider.dart';

class SocialMediaPostsHomeScreen extends StatefulWidget {
  const SocialMediaPostsHomeScreen() : super();

  @override
  _SocialMediaPostsHomeScreenState createState() =>
      _SocialMediaPostsHomeScreenState();
}

class _SocialMediaPostsHomeScreenState
    extends State<SocialMediaPostsHomeScreen> {
  String mRestaurantName = "",
      mRestaurantId = "",
      mEmployeeId = "",
      mCategoriesId = "";
  int selectedIndex = -1;

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mRestaurantName = value.employeeDetails.restaurantName;
        print(mRestaurantName);
        mRestaurantId = value.employeeDetails.restaurantId;
        mEmployeeId = value.employeeDetails.id;
        Provider.of<SocialMediaCategoriesViewModel>(context, listen: false)
            .getSocialMediaCategories(mEmployeeId, mRestaurantId);
        Provider.of<SocialMediaCategoriesViewModel>(context, listen: false)
            .getSocialMediaPosts(mCategoriesId, mEmployeeId, mRestaurantId);
      });
    });
    super.initState();
  }

  Widget postsView(SocialMediaCategoriesViewModel viewModel) {
    bool hasData = viewModel.posts.length > 0 ? true : false;
    bool havingData = viewModel.isHavingPosts ? true : false;
    return havingData
        ? (hasData
            ? new ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: viewModel.postsList.length,
                itemBuilder: (BuildContext context, int index) {
                  return SocialMediaPostsWidget(
                    postsListItem: viewModel.postsList[index],
                    selectedCatId: mCategoriesId,
                  );
                },
              )
            : Center(
                child: AutoSizeText(
                  noDataAvailable,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ))
        : Container(
            child: loader(),
          );
  }

  Widget categoriesView(SocialMediaCategoriesViewModel viewModel) {
    bool hasData = viewModel.categoriesList.length > 0 ? true : false;
    bool havingData = viewModel.isHavingCategories ? true : false;
    return havingData
        ? (hasData
            ? new Material(
                color: Colors.white,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: viewModel.categoriesList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          if (selectedIndex != index) {
                            selectedIndex = index;
                            mCategoriesId = viewModel.categoriesList[index].id;
                          } else {
                            selectedIndex = -1;
                            mCategoriesId = "";
                          }
                        });
                        print(
                            "categoryId $selectedIndex === $index $mCategoriesId");
                        viewModel.getSocialMediaPosts(
                            mCategoriesId, mEmployeeId, mRestaurantId);
                      },
                      child: SocialMediaCategoryWidget(
                          categoriesListItem: viewModel.categoriesList[index],
                          selectedIndex: selectedIndex,
                          index: index),
                    );
                  },
                ),
              )
            : Center(
                child: AutoSizeText(
                  noDataAvailable,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ))
        : Container(
            child: loader(),
          );
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<SocialMediaCategoriesViewModel>(context);
    return Scaffold(
      backgroundColor: Color(0xffF6F8FB),
      appBar: AppBar(
        leading: InkWell(
          customBorder: new CircleBorder(),
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back),
        ),
        backgroundColor: Color(0xff293855),
        title: Text(
          socialMediaPosts,
          style: TextStyle(
              fontSize: 18, fontFamily: "Poppins", fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        toolbarHeight: 56,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0x80101621),
            statusBarIconBrightness: Brightness.light),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          child: Column(
            children: [
              Container(
                  color: Colors.white,
                  width: ScreenSize.width(context),
                  height: ScreenSize.height(context) / 4.5,
                  child: categoriesView(viewModel)),
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(8, 10, 8, 8),
                width: ScreenSize.width(context),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        child: Image.asset(
                          "assets/images/header_bullet.png",
                          height: 20,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: AutoSizeText(
                          browseOffersTemplates,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w600,
                              color: Color(0xff3D4B65)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                    margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: postsView(viewModel)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
