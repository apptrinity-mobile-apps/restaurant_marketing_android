import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:marketing/services/basic_response.dart';
import 'package:marketing/services/repositories.dart';
import 'package:marketing/ui/social_media_posts/model/all_social_media_posts_model.dart';
import 'package:marketing/ui/social_media_posts/model/social_media_categories_response_model.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/toast.dart';

class SocialMediaCategoriesViewModel with ChangeNotifier {
  SocialMediaCategoriesViewModel();

  bool _isFetching = false;
  bool _isHavingPosts = false;
  bool _isHavingCategories = false;
  int _responseStatus = 0;

  List<SocialMediaCategoriesList> categories = [];
  List<SocialMediaPostsList> posts = [];
  var favCategory = SocialMediaCategoriesList(
      categoryName: favorites,
      createdOn: "",
      id: "0",
      image: "assets/images/social_media_default.png",
      userId: "",
      status: 1);

  bool get isFetching => _isFetching;

  bool get isHavingCategories => _isHavingCategories;

  bool get isHavingPosts => _isHavingPosts;

  int get responseStatus => _responseStatus;

  List<SocialMediaCategoriesList> get categoriesList {
    return categories;
  }

  List<SocialMediaPostsList> get postsList {
    return posts;
  }

  Future<void> getSocialMediaCategories(
      String employeeId, String restaurantId) async {
    _isFetching = true;
    _isHavingCategories = false;
    notifyListeners();
    categories.clear();
    categories.add(favCategory);
    try {
      dynamic response =
          await Repository().getSocialMediaCategories(employeeId, restaurantId);
      if (response != null) {
        SocialMediaCategoriesResponse _socialMediaCategoriesResponse =
            SocialMediaCategoriesResponse.fromJson(response);
        categories
            .addAll(_socialMediaCategoriesResponse.socialMediaCategoriesList);
        _isHavingCategories = true;
      } else {
        _isHavingCategories = false;
      }
    } catch (e) {
      print("error $e");
      _isHavingCategories = false;
    }
    _isFetching = false;
    notifyListeners();
  }

  Future<void> getSocialMediaPosts(
      String categoriesId, String employeeId, String restaurantId) async {
    _isFetching = true;
    _isHavingPosts = false;
    notifyListeners();
    posts.clear();
    try {
      dynamic response = await Repository()
          .getPostsBasedOnCategory(categoriesId, employeeId, restaurantId);
      if (response != null) {
        SocialMediaPostsResponse _socialMediaCategoriesResponse =
            SocialMediaPostsResponse.fromJson(response);
        posts.addAll(_socialMediaCategoriesResponse.socialMediaPostsList);
        _isHavingPosts = true;
      } else {
        _isHavingPosts = false;
      }
    } catch (e) {
      _isHavingPosts = false;
    }
    _isFetching = false;
    notifyListeners();
  }

  Future<bool> favouritePost(
      String socialMediaPostsId, String employeeId, String restaurantId) async {
    _isFetching = true;
    bool isFavourite = false;
    notifyListeners();
    try {
      dynamic response = await Repository()
          .favourite(socialMediaPostsId, employeeId, restaurantId);
      if (response != null) {
        BasicResponse _response = BasicResponse.fromJson(response);
        _responseStatus = _response.responseStatus;
        if(_responseStatus == 1) {
          isFavourite = true;
        } else {
          isFavourite = false;
        }
        Toaster().show(_response.result);
      } else {
        isFavourite = false;
      }
    } catch (e) {
      isFavourite = false;
    }
    _isFetching = false;
    notifyListeners();
    return isFavourite;
  }

  Future<bool> unFavouritePost(
      String socialMediaPostsId, String employeeId, String restaurantId) async {
    _isFetching = true;
    bool isUnFavourite = false;
    notifyListeners();
    try {
      dynamic response = await Repository()
          .unFavourite(socialMediaPostsId, employeeId, restaurantId);
      if (response != null) {
        BasicResponse _response = BasicResponse.fromJson(response);
        _responseStatus = _response.responseStatus;
        if(_responseStatus == 1) {
          isUnFavourite = true;
        } else {
          isUnFavourite = false;
        }
        Toaster().show(_response.result);
      } else {
        isUnFavourite = false;
      }
    } catch (e) {
      isUnFavourite = false;
    }
    _isFetching = false;
    notifyListeners();
    return isUnFavourite;
  }
}
