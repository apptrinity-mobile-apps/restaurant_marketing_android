import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:marketing/ui/business_creatives/model/all_promos_offers_model.dart';
import 'package:marketing/ui/edit_share/screens/edit_share_screen.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/utils/snack_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class OffersAndDesignTemplateWidget extends StatefulWidget {
  final OffersAndPromotionsList offersAndPromotionsListItem;

  const OffersAndDesignTemplateWidget(
      {Key? key, required this.offersAndPromotionsListItem})
      : super(key: key);

  @override
  _OffersAndDesignTemplateWidgetState createState() =>
      _OffersAndDesignTemplateWidgetState();
}

class _OffersAndDesignTemplateWidgetState
    extends State<OffersAndDesignTemplateWidget> {
  late OffersAndPromotionsList item;
  List<String> imagePaths = [];
  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    setState(() {
      item = widget.offersAndPromotionsListItem;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenSize.width(context) / 1,
      height: ScreenSize.height(context) / 1.95,
      margin: EdgeInsets.fromLTRB(8, 0, 8, 8),
      child: Screenshot(
          controller: screenshotController,
          child: Card(
            elevation: 3,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        placeholder: (context, url) => loader(),
                        imageUrl: item.defaultBackgroundImage,
                        errorWidget: (context, url, error) => Icon(
                          Icons.error_outline,
                          size: 50,
                        ),
                        fit: BoxFit.fill,
                        filterQuality: FilterQuality.medium,
                      ),
                    ),
                  ),
                  flex: 2,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            EditAndShareScreen(
                                                offerId: item.id)));
                              },
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Image.asset(
                                  "assets/images/edit.png",
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5),
                              child: AutoSizeText(
                                edit,
                                style: TextStyle(
                                    color: Color(0xff3D4B65),
                                    fontSize: 13,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            InkWell(
                              customBorder: new CircleBorder(),
                              onTap: () {
                                systemShare();
                              },
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Image.asset(
                                  "assets/images/share.png",
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5),
                              child: AutoSizeText(
                                share,
                                style: TextStyle(
                                    color: Color(0xff3D4B65),
                                    fontSize: 13,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            InkWell(
                              customBorder: new CircleBorder(),
                              onTap: () {
                                whatsappShare();
                              },
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Image.asset(
                                  "assets/images/whatsapp.png",
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5),
                              child: AutoSizeText(
                                whatsApp,
                                style: TextStyle(
                                    color: Color(0xff3D4B65),
                                    fontSize: 13,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  systemShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
            pixelRatio: ScreenSize.pixelRatio(context),
            delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      setState(() {
        imagePaths.clear();
        imagePaths.add(imgFile.path);
      });
      final RenderBox box = context.findRenderObject() as RenderBox;
      Share.shareFiles(imagePaths,
          text: "Sharing image",
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });
  }

  whatsappShare() async {
    showSnackBar(context, pleaseWait);
    final directory = (await getApplicationDocumentsDirectory()).path;
    File imgFile = File(directory);
    await screenshotController
        .capture(
            pixelRatio: ScreenSize.pixelRatio(context),
            delay: Duration(milliseconds: 10))
        .then((Uint8List? image) {
      //Capture Done
      imgFile = new File('$directory/zing_marketing.png');
      imgFile.writeAsBytes(image!);
      FlutterShareMe()
          .shareToWhatsApp(
          msg: "Sharing image",
          imagePath: imgFile.path,
          fileType: FileType.image)
          .then((value) {
        if (value!.toString().contains("PlatformException")) {
          showSnackBar(context, unableToShareWhatsApp);
        }
      });
    }).catchError((onError) {
      showSnackBar(context, tryAgain);
    });

  }
}
