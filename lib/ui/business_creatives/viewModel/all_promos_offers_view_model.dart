import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:marketing/services/repositories.dart';
import 'package:marketing/ui/business_creatives/model/all_promos_offers_model.dart';

class AllPromosOffersViewModel with ChangeNotifier {
  AllPromosOffersViewModel();

  bool _isFetching = false;
  bool _isHavingOffersAndPromos = false;
  int _responseStatus = 0;
  List<OffersAndPromotionsList> offersAndPromos = [];

  List<OffersAndPromotionsList> get offersAndPromosList {
    return offersAndPromos;
  }

  bool get isFetching => _isFetching;

  bool get isHavingOffersAndPromos => _isHavingOffersAndPromos;

  int get responseStatus => _responseStatus;

  Future<void> getAllPromosOffers(
      String employeeId, String restaurantId) async {
    _isFetching = true;
    _isHavingOffersAndPromos = false;
    notifyListeners();
    offersAndPromos.clear();
    try {
      dynamic response =
      await Repository().getAllPromosOffers(employeeId, restaurantId);
      if (response != null) {
        AllOffersPromosResponse _allOffersPromosResponse =
        AllOffersPromosResponse.fromJson(response);
        offersAndPromos.addAll(_allOffersPromosResponse.offersAndPromotionsList);
        _isHavingOffersAndPromos = true;
      } else {
        _isHavingOffersAndPromos = false;
      }
    } catch (e) {
      print("error $e");
      _isHavingOffersAndPromos = false;
    }
    _isFetching = false;
    notifyListeners();
  }
}
