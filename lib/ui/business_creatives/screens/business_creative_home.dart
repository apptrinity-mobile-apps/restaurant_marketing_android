import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:marketing/ui/business_creatives/viewModel/all_promos_offers_view_model.dart';
import 'package:marketing/ui/business_creatives/widgets/item_offers_design_templates.dart';
import 'package:marketing/ui/product_details/screens/product_detail_screen.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:provider/provider.dart';

class BusinessCreativeHomeScreen extends StatefulWidget {
  const BusinessCreativeHomeScreen() : super();

  @override
  _BusinessCreativeHomeScreenState createState() =>
      _BusinessCreativeHomeScreenState();
}

class _BusinessCreativeHomeScreenState
    extends State<BusinessCreativeHomeScreen> {
  String mRestaurantId = "", mEmployeeId = "";

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mRestaurantId = value.employeeDetails.restaurantId;
        mEmployeeId = value.employeeDetails.id;
        Provider.of<AllPromosOffersViewModel>(context, listen: false)
            .getAllPromosOffers(mEmployeeId, mRestaurantId);
      });
    });
    super.initState();
  }

  Widget promosOffersView(AllPromosOffersViewModel viewModel) {
    bool hasData = viewModel.offersAndPromosList.length > 0 ? true : false;
    bool havingData = viewModel.isHavingOffersAndPromos ? true : false;
    return havingData
        ? (hasData
            ? new ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: viewModel.offersAndPromosList.length,
                itemBuilder: (BuildContext context, int index) {
                  return OffersAndDesignTemplateWidget(
                    offersAndPromotionsListItem:
                        viewModel.offersAndPromosList[index],
                  );
                },
              )
            : Center(
                child: AutoSizeText(
                  noDataAvailable,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ))
        : Container(
            child: loader(),
          );
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<AllPromosOffersViewModel>(context);
    return Scaffold(
      backgroundColor: Color(0xffF6F8FB),
      appBar: AppBar(
        leading: InkWell(
          customBorder: new CircleBorder(),
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back),
        ),
        backgroundColor: Color(0xff293855),
        title: Text(
          headerBusinessCreative,
          style: TextStyle(
              fontSize: 18, fontFamily: "Poppins", fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        toolbarHeight: 56,
        elevation: 1,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0x80101621),
            statusBarIconBrightness: Brightness.light),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          child: Column(
            children: [
              Container(
                width: ScreenSize.width(context),
                margin: EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: Card(
                  elevation: 3,
                  color: Color(0xff283F6A),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductDetailScreen(
                                    text: "new_launch",
                                  )));
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 8, 10, 8),
                      child: Padding(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.fromLTRB(5, 5, 5, 1),
                                    child: AutoSizeText(
                                      newLaunchesBestSellers,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xffF3F5F8)),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(5, 1, 5, 5),
                                    child: AutoSizeText(
                                      template,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w500,
                                          color: Color(0xff64B5F6)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Spacer(),
                            Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Card(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                    child: Container(
                                        padding: EdgeInsets.all(8),
                                        child: Image.asset(
                                          "assets/images/next_arrow_black.png",
                                          height: 20,
                                          width: 20,
                                        )),
                                  ),
                                )),
                          ],
                        ),
                        padding: EdgeInsets.fromLTRB(10, 12, 10, 12),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: ScreenSize.width(context),
                margin: EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: Card(
                    elevation: 3,
                    color: Color(0xff64B5F6),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductDetailScreen(
                                      text: "product_discount",
                                    )));
                      },
                      child: Container(
                        margin: EdgeInsets.fromLTRB(10, 8, 10, 8),
                        child: Padding(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.fromLTRB(5, 5, 5, 1),
                                      child: AutoSizeText(
                                        productDiscount,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xffF3F5F8)),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(5, 1, 5, 5),
                                      child: AutoSizeText(
                                        template,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff293855)),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Spacer(),
                              Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Card(
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      child: Container(
                                          padding: EdgeInsets.all(8),
                                          child: Image.asset(
                                            "assets/images/next_arrow_blue.png",
                                            height: 20,
                                            width: 20,
                                          )),
                                    ),
                                  )),
                            ],
                          ),
                          padding: EdgeInsets.fromLTRB(10, 12, 10, 12),
                        ),
                      ),
                    )),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                margin: EdgeInsets.fromLTRB(8, 8, 8, 0),
                width: ScreenSize.width(context),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        child: Image.asset(
                          "assets/images/header_bullet.png",
                          height: 20,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: AutoSizeText(
                          browseOffersTemplates,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w600,
                              color: Color(0xff3D4B65)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: promosOffersView(viewModel),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
