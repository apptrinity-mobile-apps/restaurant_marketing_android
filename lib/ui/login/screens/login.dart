import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:marketing/ui/home/screens/home_screen.dart';
import 'package:marketing/ui/login/model/login_response_model.dart';
import 'package:marketing/ui/login/viewModel/login_view_model.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:marketing/utils/snack_bar.dart';
import 'package:marketing/utils/toast.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen() : super();

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late String mUserName = "", mPassword = "";
  TextEditingController userNameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool loginPressed = false;

  @override
  void initState() {
    userNameController.text = "";
    passwordController.text = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final LoginViewModel loginViewModel =
        Provider.of<LoginViewModel>(context, listen: false);

    void loginView(bool havingData, LoginViewModel viewModel) {
      LoginResponse? _loginResponse;
      if (viewModel.loginResponse != null) {
        setState(() {
          _loginResponse = viewModel.loginResponse;
        });
      }
      if (havingData) {
        if (_loginResponse!.responseStatus == 1) {
          showSnackBar(context, loginViewModel.loginResponse!.result);
          SessionManager().login(true);
          SessionManager().saveUserDetails(jsonEncode(loginViewModel.loginResponse));
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
        } else {
          showSnackBar(context, loginViewModel.loginResponse!.result);
          setState(() {
            loginPressed = false;
          });
        }
      } else {
        showSnackBar(context, tryAgain);
        setState(() {
          loginPressed = false;
        });
      }
    }

    return Scaffold(
        backgroundColor: Color(0xffF6F8FB),
        appBar: AppBar(
          elevation: 0,
          toolbarHeight: 0,
          systemOverlayStyle: SystemUiOverlayStyle(
              statusBarColor: Color(0xffF6F8FB),
              statusBarIconBrightness: Brightness.dark),
          backwardsCompatibility: false,
        ),
        body: SafeArea(
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(8),
               child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                       Container(
                         padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                          child: Image.asset(
                            "assets/images/restaurant_default.png",
                            fit: BoxFit.fill,
                            height: 150,
                            width: 150,
                          ),
                        ),
                      Container(
                        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                        margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: AutoSizeText(
                                  userName,
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                margin: EdgeInsets.fromLTRB(8, 0, 8, 5),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xffBBC3CF)),
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: TextField(
                                        controller: userNameController,
                                        textInputAction: TextInputAction.next,
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            isDense: true,
                                            hintText: userName,
                                            border: InputBorder.none,
                                            labelStyle: TextStyle(
                                              fontSize: 16,
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w600,
                                            )),
                                      ))
                                    ],
                                  ),
                                ),
                              ),
                            ]),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                        margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: AutoSizeText(
                                  password,
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                margin: EdgeInsets.fromLTRB(8, 0, 8, 5),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xffBBC3CF)),
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: TextField(
                                        controller: passwordController,
                                        textInputAction: TextInputAction.done,
                                        obscureText: true,
                                        /*obscuringCharacter: "*",*/
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            isDense: true,
                                            hintText: password,
                                            border: InputBorder.none,
                                            labelStyle: TextStyle(
                                              fontSize: 16,
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w600,
                                            )),
                                      ))
                                    ],
                                  ),
                                ),
                              ),
                            ]),
                      ),
                      loginPressed
                          ? Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.all(8),
                              child: SpinKitCircle(
                                color: Colors.lightBlueAccent,
                                size: 60.0,
                              ))
                          : Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.all(8),
                              child: InkWell(
                                onTap: () async {
                                  FocusScope.of(context).requestFocus(FocusNode());
                                  print(
                                      "===${userNameController.text}=====${passwordController.text}");
                                  if (userNameController.text == "") {
                                    Toaster().show(emptyUserName);
                                  } else if (passwordController.text == "") {
                                    Toaster().show(emptyPassword);
                                  } /*else if (!validatePassword(
                                      passwordController.text)) {
                                    Toaster().show(invalidPassword);
                                  } */else {
                                    setState(() {
                                      mUserName = userNameController.text;
                                      mPassword = passwordController.text;
                                      loginPressed = true;
                                    });
                                    showSnackBar(context, pleaseWait);
                                    await loginViewModel.userLogin(mUserName, mPassword).then((value) {
                                      loginView( value!, loginViewModel);
                                    });
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.all(2),
                                  decoration: ShapeDecoration(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5)),
                                    gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [Color(0xff1DC5E9), Color(0xff1EE1C3)],
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                    child: AutoSizeText(
                                      login,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ),
                            )
              ],
            ),
          ),
        )));
  }

}
