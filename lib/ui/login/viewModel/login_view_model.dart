import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:marketing/services/repositories.dart';
import 'package:marketing/ui/login/model/login_response_model.dart';

class LoginViewModel with ChangeNotifier {
  LoginViewModel();

  bool _isFetching = false;
  bool _isHavingData = false;
  LoginResponse? _loginResponse;

  LoginResponse? get loginResponse {
    return _loginResponse;
  }

  bool get isFetching => _isFetching;

  bool get isHavingData => _isHavingData;

  Future<bool?> userLogin(String username, String password) async {
    _isFetching = true;
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository().userLogin(username, password);
      if (response != null) {
        _loginResponse = LoginResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("fetch $_isFetching $_isHavingData $_loginResponse");
    notifyListeners();
    return _isHavingData;
  }
}
