class LoginResponse {
  late EmployeeDetails employeeDetails;
  late int responseStatus;
  late String result;

  LoginResponse();

  LoginResponse.fromJson(Map<String, dynamic> json) {
    employeeDetails = (json['employeeDetails'] != null
        ? new EmployeeDetails.fromJson(json['employeeDetails'])
        : null)!;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['employeeDetails'] = this.employeeDetails.toJson();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class EmployeeDetails {
  late String currency;
  late String currencyCode;
  late String currencyId;
  late String currencySymbol;
  late String empEmail;
  late String empPasscode;
  late String employeeName;
  late String firstName;
  late String franchiseId;
  late String id;
  late List<String> jobList;
  late String lastName;
  late PermissionList permissionList;
  late String phoneNumber;
  late String restaurantId;
  late String restaurantName;
  late String restaurantLogo;
  late int status;
  late String uniqueNumber;

  EmployeeDetails(
      {required this.currency,
      required this.currencyCode,
      required this.currencyId,
      required this.currencySymbol,
      required this.empEmail,
      required this.empPasscode,
      required this.employeeName,
      required this.firstName,
      required this.franchiseId,
      required this.id,
      required this.jobList,
      required this.lastName,
      required this.permissionList,
      required this.phoneNumber,
      required this.restaurantId,
      required this.restaurantName,
      required this.status,
      required this.uniqueNumber});

  EmployeeDetails.fromJson(Map<String, dynamic> json) {
    currency = json['currency'];
    currencyCode = json['currencyCode'];
    currencyId = json['currencyId'];
    currencySymbol = json['currencySymbol'];
    empEmail = json['empEmail'];
    empPasscode = json['empPasscode'] == null ? "" : json['empPasscode'];
    employeeName = json['employeeName'];
    firstName = json['firstName'];
    franchiseId = json['franchiseId'];
    id = json['id'];
    jobList = json['jobList'].cast<String>();
    lastName = json['lastName'];
    permissionList = (json['permissionList'] != null
        ? new PermissionList.fromJson(json['permissionList'])
        : null)!;
    phoneNumber = json['phoneNumber'];
    restaurantId = json['restaurantId'];
    restaurantName = json['restaurantName'];
    restaurantLogo = json['restaurantLogo'] == null ? "" : json['restaurantLogo'];
    status = json['status'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currency'] = this.currency;
    data['currencyCode'] = this.currencyCode;
    data['currencyId'] = this.currencyId;
    data['currencySymbol'] = this.currencySymbol;
    data['empEmail'] = this.empEmail;
    data['empPasscode'] = this.empPasscode;
    data['employeeName'] = this.employeeName;
    data['firstName'] = this.firstName;
    data['franchiseId'] = this.franchiseId;
    data['id'] = this.id;
    data['jobList'] = this.jobList;
    data['lastName'] = this.lastName;
    data['permissionList'] = this.permissionList.toJson();
    data['phoneNumber'] = this.phoneNumber;
    data['restaurantId'] = this.restaurantId;
    data['restaurantName'] = this.restaurantName;
    data['restaurantLogo'] = this.restaurantLogo;
    data['status'] = this.status;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

class PermissionList {
  late AccountAdminAccess accountAdminAccess;
  late DeliveryAccess deliveryAccess;
  late DeviceSetupAccess deviceSetupAccess;
  late ManagerAccess managerAccess;
  late PosAccess posAccess;
  late QuickEditAccess quickEditAccess;
  late RestaurantAdminAccess restaurantAdminAccess;
  late WebSetupAccess webSetupAccess;

  PermissionList(
      {required this.accountAdminAccess,
      required this.deliveryAccess,
      required this.deviceSetupAccess,
      required this.managerAccess,
      required this.posAccess,
      required this.quickEditAccess,
      required this.restaurantAdminAccess,
      required this.webSetupAccess});

  PermissionList.fromJson(Map<String, dynamic> json) {
    accountAdminAccess = (json['accountAdminAccess'] != null
        ? new AccountAdminAccess.fromJson(json['accountAdminAccess'])
        : null)!;
    deliveryAccess = (json['deliveryAccess'] != null
        ? new DeliveryAccess.fromJson(json['deliveryAccess'])
        : null)!;
    deviceSetupAccess = (json['deviceSetupAccess'] != null
        ? new DeviceSetupAccess.fromJson(json['deviceSetupAccess'])
        : null)!;
    managerAccess = (json['managerAccess'] != null
        ? new ManagerAccess.fromJson(json['managerAccess'])
        : null)!;
    posAccess = (json['posAccess'] != null
        ? new PosAccess.fromJson(json['posAccess'])
        : null)!;
    quickEditAccess = (json['quickEditAccess'] != null
        ? new QuickEditAccess.fromJson(json['quickEditAccess'])
        : null)!;
    restaurantAdminAccess = (json['restaurantAdminAccess'] != null
        ? new RestaurantAdminAccess.fromJson(json['restaurantAdminAccess'])
        : null)!;
    webSetupAccess = (json['webSetupAccess'] != null
        ? new WebSetupAccess.fromJson(json['webSetupAccess'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountAdminAccess'] = this.accountAdminAccess.toJson();
    data['deliveryAccess'] = this.deliveryAccess.toJson();
    data['deviceSetupAccess'] = this.deviceSetupAccess.toJson();
    data['managerAccess'] = this.managerAccess.toJson();
    data['posAccess'] = this.posAccess.toJson();
    data['quickEditAccess'] = this.quickEditAccess.toJson();
    data['restaurantAdminAccess'] = this.restaurantAdminAccess.toJson();
    data['webSetupAccess'] = this.webSetupAccess.toJson();
    return data;
  }
}

class AccountAdminAccess {
  late bool dataExportConfig;
  late bool financialAccounts;
  late bool manageIntegrations;
  late bool userPermissions;

  AccountAdminAccess(
      {required this.dataExportConfig,
      required this.financialAccounts,
      required this.manageIntegrations,
      required this.userPermissions});

  AccountAdminAccess.fromJson(Map<String, dynamic> json) {
    dataExportConfig = json['dataExportConfig'];
    financialAccounts = json['financialAccounts'];
    manageIntegrations = json['manageIntegrations'];
    userPermissions = json['userPermissions'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dataExportConfig'] = this.dataExportConfig;
    data['financialAccounts'] = this.financialAccounts;
    data['manageIntegrations'] = this.manageIntegrations;
    data['userPermissions'] = this.userPermissions;
    return data;
  }
}

class DeliveryAccess {
  late bool cancelDispatch;
  late bool completeDelivery;
  late bool deliveryMode;
  late bool dispatchDriver;
  late bool updateAllDeliveryOrders;
  late bool updateDriver;

  DeliveryAccess(
      {required this.cancelDispatch,
      required this.completeDelivery,
      required this.deliveryMode,
      required this.dispatchDriver,
      required this.updateAllDeliveryOrders,
      required this.updateDriver});

  DeliveryAccess.fromJson(Map<String, dynamic> json) {
    cancelDispatch = json['cancelDispatch'];
    completeDelivery = json['completeDelivery'];
    deliveryMode = json['deliveryMode'];
    dispatchDriver = json['dispatchDriver'];
    updateAllDeliveryOrders = json['updateAllDeliveryOrders'];
    updateDriver = json['updateDriver'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cancelDispatch'] = this.cancelDispatch;
    data['completeDelivery'] = this.completeDelivery;
    data['deliveryMode'] = this.deliveryMode;
    data['dispatchDriver'] = this.dispatchDriver;
    data['updateAllDeliveryOrders'] = this.updateAllDeliveryOrders;
    data['updateDriver'] = this.updateDriver;
    return data;
  }
}

class DeviceSetupAccess {
  late bool advancedTerminalSetup;
  late bool kdsAndOrderScreenSetup;
  late bool terminalSetup;

  DeviceSetupAccess(
      {required this.advancedTerminalSetup,
      required this.kdsAndOrderScreenSetup,
      required this.terminalSetup});

  DeviceSetupAccess.fromJson(Map<String, dynamic> json) {
    advancedTerminalSetup = json['advancedTerminalSetup'];
    kdsAndOrderScreenSetup = json['kdsAndOrderScreenSetup'];
    terminalSetup = json['terminalSetup'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['advancedTerminalSetup'] = this.advancedTerminalSetup;
    data['kdsAndOrderScreenSetup'] = this.kdsAndOrderScreenSetup;
    data['terminalSetup'] = this.terminalSetup;
    return data;
  }
}

class ManagerAccess {
  late bool adjustCashDrawerStartBalance;
  late bool bulkClosePaidChecks;
  late bool bulkTransferCheck;
  late bool bulkVoidOpenChecks;
  late bool cashDrawerLockdown;
  late bool cashDrawersBlind;
  late bool cashDrawersFull;
  late bool closeOutDay;
  late bool discounts;
  late bool editSentItems;
  late bool editTimeEntries;
  late bool endBreaksEarly;
  late bool findChecks;
  late bool giftCardAdjustment;
  late bool largeCashOverOrUnder;
  late bool logBook;
  late bool negativeDeclaredTips;
  late bool openItems;
  late bool otherPaymentTypes;
  late bool payout;
  late bool registerSwipeCards;
  late bool sendNotifications;
  late bool shiftReview;
  late bool taxExempt;
  late bool throttleOnlineOrders;
  late bool transferOrRewardsAdjustment;
  late bool unlinkedRefunds;
  late bool voidItemOrOrders;
  late bool voidOrRefundPayments;

  ManagerAccess(
      {required this.adjustCashDrawerStartBalance,
      required this.bulkClosePaidChecks,
      required this.bulkTransferCheck,
      required this.bulkVoidOpenChecks,
      required this.cashDrawerLockdown,
      required this.cashDrawersBlind,
      required this.cashDrawersFull,
      required this.closeOutDay,
      required this.discounts,
      required this.editSentItems,
      required this.editTimeEntries,
      required this.endBreaksEarly,
      required this.findChecks,
      required this.giftCardAdjustment,
      required this.largeCashOverOrUnder,
      required this.logBook,
      required this.negativeDeclaredTips,
      required this.openItems,
      required this.otherPaymentTypes,
      required this.payout,
      required this.registerSwipeCards,
      required this.sendNotifications,
      required this.shiftReview,
      required this.taxExempt,
      required this.throttleOnlineOrders,
      required this.transferOrRewardsAdjustment,
      required this.unlinkedRefunds,
      required this.voidItemOrOrders,
      required this.voidOrRefundPayments});

  ManagerAccess.fromJson(Map<String, dynamic> json) {
    adjustCashDrawerStartBalance = json['adjustCashDrawerStartBalance'];
    bulkClosePaidChecks = json['bulkClosePaidChecks'];
    bulkTransferCheck = json['bulkTransferCheck'];
    bulkVoidOpenChecks = json['bulkVoidOpenChecks'];
    cashDrawerLockdown = json['cashDrawerLockdown'];
    cashDrawersBlind = json['cashDrawersBlind'];
    cashDrawersFull = json['cashDrawersFull'];
    closeOutDay = json['closeOutDay'];
    discounts = json['discounts'];
    editSentItems = json['editSentItems'];
    editTimeEntries = json['editTimeEntries'];
    endBreaksEarly = json['endBreaksEarly'];
    findChecks = json['findChecks'];
    giftCardAdjustment = json['giftCardAdjustment'];
    largeCashOverOrUnder = json['largeCashOverOrUnder'];
    logBook = json['logBook'];
    negativeDeclaredTips = json['negativeDeclaredTips'];
    openItems = json['openItems'];
    otherPaymentTypes = json['otherPaymentTypes'];
    payout = json['payout'];
    registerSwipeCards = json['registerSwipeCards'];
    sendNotifications = json['sendNotifications'];
    shiftReview = json['shiftReview'];
    taxExempt = json['taxExempt'];
    throttleOnlineOrders = json['throttleOnlineOrders'];
    transferOrRewardsAdjustment = json['transferOrRewardsAdjustment'];
    unlinkedRefunds = json['unlinkedRefunds'];
    voidItemOrOrders = json['voidItemOrOrders'];
    voidOrRefundPayments = json['voidOrRefundPayments'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adjustCashDrawerStartBalance'] = this.adjustCashDrawerStartBalance;
    data['bulkClosePaidChecks'] = this.bulkClosePaidChecks;
    data['bulkTransferCheck'] = this.bulkTransferCheck;
    data['bulkVoidOpenChecks'] = this.bulkVoidOpenChecks;
    data['cashDrawerLockdown'] = this.cashDrawerLockdown;
    data['cashDrawersBlind'] = this.cashDrawersBlind;
    data['cashDrawersFull'] = this.cashDrawersFull;
    data['closeOutDay'] = this.closeOutDay;
    data['discounts'] = this.discounts;
    data['editSentItems'] = this.editSentItems;
    data['editTimeEntries'] = this.editTimeEntries;
    data['endBreaksEarly'] = this.endBreaksEarly;
    data['findChecks'] = this.findChecks;
    data['giftCardAdjustment'] = this.giftCardAdjustment;
    data['largeCashOverOrUnder'] = this.largeCashOverOrUnder;
    data['logBook'] = this.logBook;
    data['negativeDeclaredTips'] = this.negativeDeclaredTips;
    data['openItems'] = this.openItems;
    data['otherPaymentTypes'] = this.otherPaymentTypes;
    data['payout'] = this.payout;
    data['registerSwipeCards'] = this.registerSwipeCards;
    data['sendNotifications'] = this.sendNotifications;
    data['shiftReview'] = this.shiftReview;
    data['taxExempt'] = this.taxExempt;
    data['throttleOnlineOrders'] = this.throttleOnlineOrders;
    data['transferOrRewardsAdjustment'] = this.transferOrRewardsAdjustment;
    data['unlinkedRefunds'] = this.unlinkedRefunds;
    data['voidItemOrOrders'] = this.voidItemOrOrders;
    data['voidOrRefundPayments'] = this.voidOrRefundPayments;
    return data;
  }
}

class PosAccess {
  late bool addOrUpdateServiceCharges;
  late bool applyCashPayments;
  late bool cashDrawerAccess;
  late bool changeServer;
  late bool changeTable;
  late bool editOtherEmployeesOrders;
  late bool keyInCreditCards;
  late bool kitchenDisplayScreenMode;
  late bool myReports;
  late bool noSale;
  late bool offlineOrBackgroundCreditCardProcessing;
  late bool pandingOrdersMode;
  late bool paymentTerminalMode;
  late bool quickOrderMode;
  late bool shiftReviewSalesData;
  late bool tableServiceMode;
  late bool viewOtherEmployeesOrders;

  PosAccess(
      {required this.addOrUpdateServiceCharges,
      required this.applyCashPayments,
      required this.cashDrawerAccess,
      required this.changeServer,
      required this.changeTable,
      required this.editOtherEmployeesOrders,
      required this.keyInCreditCards,
      required this.kitchenDisplayScreenMode,
      required this.myReports,
      required this.noSale,
      required this.offlineOrBackgroundCreditCardProcessing,
      required this.pandingOrdersMode,
      required this.paymentTerminalMode,
      required this.quickOrderMode,
      required this.shiftReviewSalesData,
      required this.tableServiceMode,
      required this.viewOtherEmployeesOrders});

  PosAccess.fromJson(Map<String, dynamic> json) {
    addOrUpdateServiceCharges = json['addOrUpdateServiceCharges'];
    applyCashPayments = json['applyCashPayments'];
    cashDrawerAccess = json['cashDrawerAccess'];
    changeServer = json['changeServer'];
    changeTable = json['changeTable'];
    editOtherEmployeesOrders = json['editOtherEmployeesOrders'];
    keyInCreditCards = json['keyInCreditCards'];
    kitchenDisplayScreenMode = json['kitchenDisplayScreenMode'];
    myReports = json['myReports'];
    noSale = json['noSale'];
    offlineOrBackgroundCreditCardProcessing =
        json['offlineOrBackgroundCreditCardProcessing'];
    pandingOrdersMode = json['pandingOrdersMode'];
    paymentTerminalMode = json['paymentTerminalMode'];
    quickOrderMode = json['quickOrderMode'];
    shiftReviewSalesData = json['shiftReviewSalesData'];
    tableServiceMode = json['tableServiceMode'];
    viewOtherEmployeesOrders = json['viewOtherEmployeesOrders'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addOrUpdateServiceCharges'] = this.addOrUpdateServiceCharges;
    data['applyCashPayments'] = this.applyCashPayments;
    data['cashDrawerAccess'] = this.cashDrawerAccess;
    data['changeServer'] = this.changeServer;
    data['changeTable'] = this.changeTable;
    data['editOtherEmployeesOrders'] = this.editOtherEmployeesOrders;
    data['keyInCreditCards'] = this.keyInCreditCards;
    data['kitchenDisplayScreenMode'] = this.kitchenDisplayScreenMode;
    data['myReports'] = this.myReports;
    data['noSale'] = this.noSale;
    data['offlineOrBackgroundCreditCardProcessing'] =
        this.offlineOrBackgroundCreditCardProcessing;
    data['pandingOrdersMode'] = this.pandingOrdersMode;
    data['paymentTerminalMode'] = this.paymentTerminalMode;
    data['quickOrderMode'] = this.quickOrderMode;
    data['shiftReviewSalesData'] = this.shiftReviewSalesData;
    data['tableServiceMode'] = this.tableServiceMode;
    data['viewOtherEmployeesOrders'] = this.viewOtherEmployeesOrders;
    return data;
  }
}

class QuickEditAccess {
  late bool addExistingItemsOrMods;
  late bool addNewItemsMods;
  late bool buttonColor;
  late bool fullQuickEdit;
  late bool inventoryAndQuantity;
  late bool name;
  late bool posName;
  late bool price;
  late bool rearrangingItemsMods;
  late bool removeItemsOrMods;
  late bool sku;

  QuickEditAccess(
      {required this.addExistingItemsOrMods,
      required this.addNewItemsMods,
      required this.buttonColor,
      required this.fullQuickEdit,
      required this.inventoryAndQuantity,
      required this.name,
      required this.posName,
      required this.price,
      required this.rearrangingItemsMods,
      required this.removeItemsOrMods,
      required this.sku});

  QuickEditAccess.fromJson(Map<String, dynamic> json) {
    addExistingItemsOrMods = json['addExistingItemsOrMods'];
    addNewItemsMods = json['addNewItemsMods'];
    buttonColor = json['buttonColor'];
    fullQuickEdit = json['fullQuickEdit'];
    inventoryAndQuantity = json['inventoryAndQuantity'];
    name = json['name'];
    posName = json['posName'];
    price = json['price'];
    rearrangingItemsMods = json['rearrangingItemsMods'];
    removeItemsOrMods = json['removeItemsOrMods'];
    sku = json['sku'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addExistingItemsOrMods'] = this.addExistingItemsOrMods;
    data['addNewItemsMods'] = this.addNewItemsMods;
    data['buttonColor'] = this.buttonColor;
    data['fullQuickEdit'] = this.fullQuickEdit;
    data['inventoryAndQuantity'] = this.inventoryAndQuantity;
    data['name'] = this.name;
    data['posName'] = this.posName;
    data['price'] = this.price;
    data['rearrangingItemsMods'] = this.rearrangingItemsMods;
    data['removeItemsOrMods'] = this.removeItemsOrMods;
    data['sku'] = this.sku;
    return data;
  }
}

class RestaurantAdminAccess {
  late bool customersCreditsAndReports;
  late bool editFullMenu;
  late bool editHistoricalData;
  late bool employeeInfo;
  late bool employeeJobsAndWages;
  late bool giftOrRewardsCardReport;
  late bool houseAccounts;
  late bool laborReports;
  late bool localMenuEdit;
  late bool marketingInfo;
  late bool menuReports;
  late bool salesReports;
  late bool tables;

  RestaurantAdminAccess(
      {required this.customersCreditsAndReports,
      required this.editFullMenu,
      required this.editHistoricalData,
      required this.employeeInfo,
      required this.employeeJobsAndWages,
      required this.giftOrRewardsCardReport,
      required this.houseAccounts,
      required this.laborReports,
      required this.localMenuEdit,
      required this.marketingInfo,
      required this.menuReports,
      required this.salesReports,
      required this.tables});

  RestaurantAdminAccess.fromJson(Map<String, dynamic> json) {
    customersCreditsAndReports = json['customersCreditsAndReports'];
    editFullMenu = json['editFullMenu'];
    editHistoricalData = json['editHistoricalData'];
    employeeInfo = json['employeeInfo'];
    employeeJobsAndWages = json['employeeJobsAndWages'];
    giftOrRewardsCardReport = json['giftOrRewardsCardReport'];
    houseAccounts = json['houseAccounts'];
    laborReports = json['laborReports'];
    localMenuEdit = json['localMenuEdit'];
    marketingInfo = json['marketingInfo'];
    menuReports = json['menuReports'];
    salesReports = json['salesReports'];
    tables = json['tables'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customersCreditsAndReports'] = this.customersCreditsAndReports;
    data['editFullMenu'] = this.editFullMenu;
    data['editHistoricalData'] = this.editHistoricalData;
    data['employeeInfo'] = this.employeeInfo;
    data['employeeJobsAndWages'] = this.employeeJobsAndWages;
    data['giftOrRewardsCardReport'] = this.giftOrRewardsCardReport;
    data['houseAccounts'] = this.houseAccounts;
    data['laborReports'] = this.laborReports;
    data['localMenuEdit'] = this.localMenuEdit;
    data['marketingInfo'] = this.marketingInfo;
    data['menuReports'] = this.menuReports;
    data['salesReports'] = this.salesReports;
    data['tables'] = this.tables;
    return data;
  }
}

class WebSetupAccess {
  late bool dataExportConfig;
  late bool discountsSetup;
  late bool financialAccounts;
  late bool kitchenOrDiningRoomSetup;
  late bool manageInstructions;
  late bool paymentsSetup;
  late bool publishing;
  late bool restaurantGroupsSetup;
  late bool restaurantOperationsSetup;
  late bool taxRatesSetup;
  late bool userPermissions;

  WebSetupAccess(
      {required this.dataExportConfig,
      required this.discountsSetup,
      required this.financialAccounts,
      required this.kitchenOrDiningRoomSetup,
      required this.manageInstructions,
      required this.paymentsSetup,
      required this.publishing,
      required this.restaurantGroupsSetup,
      required this.restaurantOperationsSetup,
      required this.taxRatesSetup,
      required this.userPermissions});

  WebSetupAccess.fromJson(Map<String, dynamic> json) {
    dataExportConfig = json['dataExportConfig'];
    discountsSetup = json['discountsSetup'];
    financialAccounts = json['financialAccounts'];
    kitchenOrDiningRoomSetup = json['kitchenOrDiningRoomSetup'];
    manageInstructions = json['manageInstructions'];
    paymentsSetup = json['paymentsSetup'];
    publishing = json['publishing'];
    restaurantGroupsSetup = json['restaurantGroupsSetup'];
    restaurantOperationsSetup = json['restaurantOperationsSetup'];
    taxRatesSetup = json['taxRatesSetup'];
    userPermissions = json['userPermissions'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dataExportConfig'] = this.dataExportConfig;
    data['discountsSetup'] = this.discountsSetup;
    data['financialAccounts'] = this.financialAccounts;
    data['kitchenOrDiningRoomSetup'] = this.kitchenOrDiningRoomSetup;
    data['manageInstructions'] = this.manageInstructions;
    data['paymentsSetup'] = this.paymentsSetup;
    data['publishing'] = this.publishing;
    data['restaurantGroupsSetup'] = this.restaurantGroupsSetup;
    data['restaurantOperationsSetup'] = this.restaurantOperationsSetup;
    data['taxRatesSetup'] = this.taxRatesSetup;
    data['userPermissions'] = this.userPermissions;
    return data;
  }
}
