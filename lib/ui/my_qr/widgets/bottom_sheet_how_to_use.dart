import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/screen_size.dart';

class BottomSheetHowToUseWidget extends StatelessWidget {
  const BottomSheetHowToUseWidget() : super();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
          margin: EdgeInsets.fromLTRB(8, 10, 8, 10),
          width: ScreenSize.width(context),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  child: InkWell(
                    child: Image.asset(
                      "assets/images/header_bullet.png",
                      height: 30,
                      width: 10,
                    ),
                    onTap: () {},
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(padding:EdgeInsets.fromLTRB(10, 0, 10, 0),child: AutoSizeText(
                  howToUse,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w600,
                      color: Color(0xff3D4B65)),
                ),),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  child: InkWell(
                    customBorder: new CircleBorder(),
                    child: Icon(
                      Icons.close,
                      size: 30,
                      color: Colors.black,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Row(
            children: [
              Container(
                child: Image.asset(
                  "assets/images/print_qr.png",
                  height: 50,
                  width: 50,
                ),
              ),
              Expanded(child: Container(
                margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                alignment: Alignment.center,
                child: Align(
                  alignment: Alignment.center,
                  child: AutoSizeText(
                    printQrToStore,
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: 14, color: Color(0xff3D4B65),
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),)
            ],
          ),
        ),
        Container(
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Row(
            children: [
              Container(
                child: Image.asset(
                  "assets/images/my_qr_code.png",
                  height: 50,
                  width: 50,
                ),
              ),
              Expanded(child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                child: Align(
                  alignment: Alignment.center,
                  child: AutoSizeText(
                    scanQrToOpenStore,
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: 14, color: Color(0xff3D4B65),
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),)
            ],
          ),
        ),
        Container(
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Row(
            children: [
              Container(
                child: Image.asset(
                  "assets/images/store.png",
                  height: 50,
                  width: 50,
                ),
              ),
              Expanded(child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                child: Align(
                  alignment: Alignment.center,
                  child: AutoSizeText(
                    customersViewPlaceOrder,
                    textAlign: TextAlign.start,
                    maxLines: 2,
                    style: TextStyle(fontSize: 14, color: Color(0xff3D4B65),
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),)
            ],
          ),
        ),
      ],
    );
  }
}
