import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marketing/ui/my_qr/model/qr_model_response.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/utils/shared_preferences.dart';

class ItemQrCodeWidget extends StatefulWidget {
  final QrCodeImage? qrImageData;
  final Color color;

  const ItemQrCodeWidget({required this.qrImageData, required this.color})
      : super();

  @override
  _ItemQrCodeWidgetState createState() => _ItemQrCodeWidgetState();
}

class _ItemQrCodeWidgetState extends State<ItemQrCodeWidget> {
  late QrCodeImage? qrData;
  String mRestaurantLogo = "";
  late Color mColor;

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mRestaurantLogo = value.employeeDetails.restaurantLogo;
      });
    });
    setState(() {
      qrData = widget.qrImageData;
      mColor = widget.color;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenSize.width(context) / 1.03,
      height: ScreenSize.height(context),
      margin: EdgeInsets.fromLTRB(10, 8, 10, 8),
      child: Card(
        elevation: 3,
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Column(
          children: [
            Expanded(
              flex: 2,
              child: Container(
                height: ScreenSize.width(context),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  color: mColor,
                ),
                width: ScreenSize.width(context),
                child: Padding(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                AutoSizeText(
                                  qrData!.restaurantName,
                                  textAlign: TextAlign.justify,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xffF6F8FB)),
                                ),
                              ],
                            )),
                        Expanded(
                          flex: 4,
                          child: Card(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Container(
                              width: ScreenSize.width(context) / 2,
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: CachedNetworkImage(
                                      placeholder: (context, url) => loader(),
                                      imageUrl: qrData!.qrcodeImage,
                                      errorWidget: (context, url, error) =>
                                          Icon(
                                        Icons.error_outline,
                                        size: 150,
                                      ),
                                      fit: BoxFit.fill,
                                      filterQuality: FilterQuality.medium,
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                    child: AutoSizeText(
                                      scanQR,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xff64B5F6)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                AutoSizeText(
                                  qrData!.onlineUrl,
                                  textAlign: TextAlign.justify,
                                  maxLines: 2,
                                  minFontSize: 13,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Color(0xffF6F8FB),
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w400),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                  padding: EdgeInsets.all(10),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10))),
              child: (Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              child: CachedNetworkImage(
                                placeholder: (context, url) => loader(),
                                imageUrl: mRestaurantLogo,
                                errorWidget: (context, url, error) => Icon(
                                  Icons.error_outline,
                                  size: 50,
                                ),
                                fit: BoxFit.fill,
                                filterQuality: FilterQuality.medium,
                                width: 60,
                                height: 60,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.fromLTRB(10, 8, 10, 8),
                              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                              child: AutoSizeText(
                                digitalShowroom,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff3D4B65)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )),
            )
          ],
        ),
      ),
    );
  }
}
