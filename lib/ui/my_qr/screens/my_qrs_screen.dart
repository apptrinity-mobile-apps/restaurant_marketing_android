import 'dart:io';
import 'dart:typed_data';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:marketing/ui/my_qr/model/qr_model_response.dart';
import 'package:marketing/ui/my_qr/viewModel/qr_view_model.dart';
import 'package:marketing/ui/my_qr/widgets/bottom_sheet_how_to_use.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:marketing/ui/my_qr/widgets/item_my_qr_code.dart';
import 'package:marketing/utils/shared_preferences.dart';
import 'package:marketing/utils/snack_bar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:scratcher/scratcher.dart';

class MyQrsScreen extends StatefulWidget {
  const MyQrsScreen() : super();

  @override
  _MyQrsScreenState createState() => _MyQrsScreenState();
}

class _MyQrsScreenState extends State<MyQrsScreen> {
  String mRestaurantId = "";
  String mRestaurantName = "";
  List<Color> colors = [];
  GlobalKey previewContainer = new GlobalKey();
  int _index = 0;
  QrCodeImage? _qrImageData;

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mRestaurantId = value.employeeDetails.restaurantId;
        mRestaurantName = value.employeeDetails.restaurantName;
        Provider.of<QRViewModel>(context, listen: false)
            .getRestaurantQRApi(mRestaurantId);
        colors.add(qr_bg_color_red);
        colors.add(qr_bg_color_yellow);
        colors.add(qr_bg_color_blue);
        colors.add(qr_bg_color_green_shade);
      });
    });

    super.initState();
  }

  Widget loadListView(QRViewModel viewModel) {
    bool havingData = viewModel.isHavingData ? true : false;
    bool responseHasData = havingData
        ? (viewModel.qrResponse!.responseStatus == 1 ? true : false)
        : false;
    if (viewModel.qrResponse != null) {
      setState(() {
        _qrImageData = viewModel.qrResponse!.qrCodeImage;
      });
    }
    return havingData
        ? (responseHasData
            ? colors.length > 0
                ? (_qrImageData!.qrcodeImage != "")
                    ? RepaintBoundary(
                        key: previewContainer,
                        child: new PageView.builder(
                            itemCount: colors.length,
                            controller: PageController(viewportFraction: 1),
                            onPageChanged: (int index) =>
                                setState(() => _index = index),
                            itemBuilder: (BuildContext context, int index) {
                              return Transform.scale(
                                scale: index == _index ? 1 : 0.9,
                                child: ItemQrCodeWidget(
                                    qrImageData: _qrImageData,
                                    color: colors[index]),
                              );
                            }))
                    : Center(
                        child: AutoSizeText(
                          noDataAvailable,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      )
                : Center(
                    child: AutoSizeText(
                      noDataAvailable,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  )
            : Center(
                child: AutoSizeText(
                  noDataAvailable,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ))
        : Container(
            child: loader(),
          );
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<QRViewModel>(context);
    return Scaffold(
      backgroundColor: Color(0xffF6F8FB),
      appBar: AppBar(
        leading: InkWell(
          customBorder: new CircleBorder(),
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back),
        ),
        backgroundColor: Color(0xff293855),
        title: Text(
          headerMyQr,
          style: TextStyle(
              fontSize: 18, fontFamily: "Poppins", fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        toolbarHeight: 56,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0x80101621),
            statusBarIconBrightness: Brightness.light),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
          child: Container(
        height: ScreenSize.height(context),
        color: Colors.black12,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
              height: ScreenSize.height(context) / 1.65,
              child: loadListView(viewModel),
            ),
            Expanded(
                child: Column(
              children: [
                Container(
                  width: ScreenSize.width(context),
                  margin: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Card(
                      elevation: 3,
                      color: Color(0xff283F6A),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: InkWell(
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(0),
                                      topRight: Radius.circular(0))),
                              builder: (context) {
                                return new BottomSheetHowToUseWidget();
                              });
                          // showCounterPopup();
                        },
                        child: Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: Padding(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                    child: AutoSizeText(
                                      howToUse,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xffF3F5F8)),
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                      child: Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        child: Container(
                                            padding: EdgeInsets.all(8),
                                            child: Image.asset(
                                              "assets/images/next_arrow_black.png",
                                              height: 20,
                                              width: 20,
                                            )),
                                      ),
                                    )),
                              ],
                            ),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      )),
                ),
                
              ],
            )),
          ],
        ),
      )),
      bottomSheet: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            children: [
              Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: Material(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: ScreenSize.width(context) / 2.25,
                          child: Card(
                              elevation: 3,
                              color: Color(0xff293855),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: InkWell(
                                onTap: () {
                                  saveQR();
                                },
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                                  child: AutoSizeText(
                                    saveQr,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              )),
                        ),
                        Container(
                          width: ScreenSize.width(context) / 2.25,
                          child: Card(
                              elevation: 3,
                              color: Color(0xff1798FF),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: InkWell(
                                onTap: () {
                                  saveQRtoDevice();
                                },
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                                  child: AutoSizeText(
                                    saveDevice,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ],
      ),
    );
  }

  void showCounterPopup() {
    double _opacity = 0.0;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              title: Align(
                alignment: Alignment.center,
                child: Text(
                  'You\'ve won a scratch card',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
              content:
                  StatefulBuilder(builder: (context, StateSetter setState) {
                   return Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),child: Scratcher(
                      color: Colors.redAccent,
                      accuracy: ScratchAccuracy.low,
                      threshold: 50,
                      brushSize: 50,
                      onThreshold: () {
                        setState(() {
                          _opacity = 1;
                        });
                      },
                      // image: Image.asset("assets/images/social_media_banner.png"),
                      child: AnimatedOpacity(
                        duration: Duration(milliseconds: 250),
                        opacity: _opacity,
                        child: Container(
                          height: 50,
                          width: 300,
                          alignment: Alignment.center,
                          child: Text(
                            "Gift card \u0024200",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.blue),
                          ),
                        ),
                      ),
                    ), height: 75,);
              }));
          /*Center(child: CircularCountDownTimer(
            duration: 4,
            initialDuration: 0,
            controller: _controller,
            width: 250,
            height: 250,
            ringColor: Colors.grey,
            ringGradient: null,
            fillColor: Colors.purpleAccent,
            fillGradient: null,
            backgroundColor: Colors.purple[500],
            backgroundGradient: null,
            strokeWidth: 15.0,
            strokeCap: StrokeCap.butt,
            textStyle: TextStyle(
                fontSize: 33.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
            textFormat: CountdownTextFormat.S,
            isReverse: true,
            isReverseAnimation: false,
            isTimerTextShown: true,
            autoStart: true,
            onStart: () {
              print('Countdown Started');
            },
            onComplete: () {
              print('Countdown Ended');
              Navigator.pop(context);
            },
          ),)*/
        });
  }

  saveQRtoDevice() async {
    showSnackBar(context, waitSaving);
    await Permission.manageExternalStorage.request();

    if (await Permission.manageExternalStorage.status.isGranted) {
      // final path = await getExternalStorageDirectory();
      final folderName = "Zing Marketing";
      final path = Directory("storage/emulated/0/Pictures/$folderName");
      if ((await path.exists())) {
        print("exist");
      } else {
        print("not exist");
        path.create();
      }
      var dt = new DateTime.now();
      var formatter = new DateFormat('MM_dd_yyyy_HH_mm_ss');
      String formattedDate = formatter.format(dt);
      print(formattedDate);
      RenderRepaintBoundary? boundary = previewContainer.currentContext!
          .findRenderObject() as RenderRepaintBoundary?;
      final image = await boundary!.toImage(pixelRatio: 5);
      ByteData? byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData!.buffer.asUint8List();
      _save(pngBytes);
      // var imageName = mRestaurantName.replaceAll(RegExp(" "), "_");
      // File imgFile =
      //     new File(path.path + '/${imageName}_$formattedDate.png');
      // print(imgFile);
      // imgFile.writeAsBytesSync(pngBytes, mode: FileMode.write);
      // showSnackBar(saved);
    } else if (await Permission.manageExternalStorage.status.isDenied) {
      await Permission.manageExternalStorage.shouldShowRequestRationale;
    } else if (await Permission
        .manageExternalStorage.status.isPermanentlyDenied) {
      await Permission.manageExternalStorage.shouldShowRequestRationale;
    }
  }

  saveQR() async {
    showSnackBar(context, waitSaving);
    await Permission.manageExternalStorage.request();

    if (await Permission.manageExternalStorage.status.isGranted) {
      // final path = await getExternalStorageDirectory();
      final folderName = "Zing Marketing";
      final path = Directory("storage/emulated/0/Pictures/$folderName");
      if ((await path.exists())) {
        print("exist");
      } else {
        print("not exist");
        path.create(recursive: true);
      }
      var dt = new DateTime.now();
      var formatter = new DateFormat('MM_dd_yyyy_HH_mm_ss');
      String formattedDate = formatter.format(dt);
      print(formattedDate);
      ByteData? byteData =
          await NetworkAssetBundle(Uri.parse(_qrImageData!.qrcodeImage))
              .load("");
      Uint8List pngBytes = byteData.buffer.asUint8List();
      _save(pngBytes);
      /* saves image to Pictures/Zing Marketing and doesn't shows in gallery, with custom name */
      // File imgFile = new File(path.path + "/QR_$formattedDate.jpg");
      // imgFile.writeAsBytesSync(pngBytes, mode: FileMode.write);
      // print(imgFile);
      // await ImageGallerySaver.saveFile(imgFile.toString());
      // showSnackBar(saved);
    } else if (await Permission.manageExternalStorage.status.isDenied) {
      await Permission.manageExternalStorage.shouldShowRequestRationale;
    } else if (await Permission
        .manageExternalStorage.status.isPermanentlyDenied) {
      await Permission.manageExternalStorage.shouldShowRequestRationale;
    }
  }

  _save(Uint8List imgFile) async {
    /* saves image to Pictures and shows in gallery, with default name */
    final result = await ImageGallerySaver.saveImage(imgFile);
    print(result);
    showSnackBar(context, saved);
  }
}
