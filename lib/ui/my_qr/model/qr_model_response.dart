class QRResponse {
  late QrCodeImage qrCodeImage;
  late int responseStatus;
  late String result;

  QRResponse({required this.qrCodeImage, required this.responseStatus,required  this.result});

  QRResponse.fromJson(Map<String, dynamic> json) {
    qrCodeImage = (json['qrCodeImage'] != null
        ? new QrCodeImage.fromJson(json['qrCodeImage'])
        : null)!;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['qrCodeImage'] = this.qrCodeImage.toJson();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class QrCodeImage {
  late  String qrcodeImage;
  late  String restaurantId;
  late  String restaurantName;
  late  String onlineUrl;

  QrCodeImage({required this.onlineUrl, required this.qrcodeImage, required this.restaurantId, required this.restaurantName});

  QrCodeImage.fromJson(Map<String, dynamic> json) {
    qrcodeImage = json['qrcodeImage'] == null ? "" : json['qrcodeImage'];
    restaurantId = json['restaurantId'];
    restaurantName = json['restaurantName'];
    onlineUrl = json['onlineUrl'] == null ? "" : json['onlineUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['qrcodeImage'] = this.qrcodeImage;
    data['restaurantId'] = this.restaurantId;
    data['restaurantName'] = this.restaurantName;
    data['onlineUrl'] = this.onlineUrl;
    return data;
  }
}
