
import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:marketing/services/repositories.dart';
import 'package:marketing/ui/my_qr/model/qr_model_response.dart';

class QRViewModel with ChangeNotifier {
  bool _isFetching = false;
  bool _isHavingData = false;
  QRResponse? _qrResponse;

  bool get isFetching => _isFetching;

  bool get isHavingData => _isHavingData;

  QRResponse? get qrResponse {
    return _qrResponse;
  }


  Future<void> getRestaurantQRApi( String restaurantId) async {
    _isFetching = true;
    _isHavingData = false;
    _qrResponse = null;
    notifyListeners();
    try {
      dynamic response =
      await Repository().getRestaurantQR(restaurantId);
      if (response != null) {
        QRResponse _response = QRResponse.fromJson(response);
        _qrResponse = _response;
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      print("error $e");
      _isHavingData = false;
    }
    _isFetching = false;
    notifyListeners();
  }
}