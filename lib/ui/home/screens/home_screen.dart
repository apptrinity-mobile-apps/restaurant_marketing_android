import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:marketing/ui/business_creatives/screens/business_creative_home.dart';
import 'package:marketing/ui/dummy.dart';
import 'package:marketing/ui/home/widgets/bottom_sheet_share_whatsapp.dart';
import 'package:marketing/ui/my_qr/screens/my_qrs_screen.dart';
import 'package:marketing/ui/social_media_posts/screens/social_media_posts_home.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/progress_loading.dart';
import 'package:marketing/utils/screen_size.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/utils/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen() : super();

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  String mRestaurantName = "", mRestaurantId = "", mRestaurantLogo = "";

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mRestaurantName = value.employeeDetails.restaurantName;
        print(mRestaurantName);
        mRestaurantId = value.employeeDetails.restaurantId;
        mRestaurantLogo = value.employeeDetails.restaurantLogo;
      });
    });
    super.initState();
  }

  late Timer _timer;
  int _start = 3;
  int counterText = 0;
  late AnimationController _controller;
  Tween<double> _tween = Tween(begin: 0.75, end: 2);
  bool isViewShowing = true;

  void startTimer() {
    setState(() {
      isViewShowing = false;
    });
    const oneSec = const Duration(milliseconds: 1500);
    _controller = AnimationController(
        duration: const Duration(milliseconds: 750), vsync: this);
    _controller.repeat(reverse: true);
    _timer = new Timer.periodic(oneSec, (Timer timer) {
      if (_start == 0) {
        setState(() {
          timer.cancel();
          counterText = 0;
          _start = 3;
          isViewShowing = true;
        });
      } else {
        setState(() {
          _start--;
          counterText++;
        });
      }
      print(counterText);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF6F8FB),
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0xffF6F8FB),
            statusBarIconBrightness: Brightness.dark),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.all(8),
          child: isViewShowing
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                      Container(
                        width: ScreenSize.width(context),
                        height: ScreenSize.height(context) / 6.5,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Card(
                          elevation: 2,
                          color: Color(0xff293855),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  margin: EdgeInsets.all(12),
                                  height: ScreenSize.width(context),
                                  width: ScreenSize.height(context),
                                  child: Card(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: Padding(
                                      padding: EdgeInsets.all(5),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: CachedNetworkImage(
                                            placeholder: (context, url) =>
                                                loader(),
                                            imageUrl: mRestaurantLogo,
                                            errorWidget:
                                                (context, url, error) => Icon(
                                              Icons.error_outline,
                                              size: 50,
                                            ),
                                            fit: BoxFit.fill,
                                            filterQuality: FilterQuality.medium,
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: ScreenSize.width(context),
                                    width: ScreenSize.height(context),
                                    margin: EdgeInsets.fromLTRB(0, 8, 8, 8),
                                    alignment: Alignment.centerLeft,
                                    child: AutoSizeText(
                                      mRestaurantName,
                                      minFontSize: 12,
                                      maxFontSize: 18,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w600),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: ScreenSize.width(context),
                        height: ScreenSize.height(context) / 4.5,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Card(
                          elevation: 2,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: Stack(
                            children: [
                              Positioned(
                                  child: Container(
                                child: Image.asset(
                                  "assets/images/promo_banner.png",
                                  fit: BoxFit.fill,
                                ),
                              )),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                      height: ScreenSize.width(context),
                                      width: ScreenSize.height(context),
                                      child: Padding(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 0),
                                              child: AutoSizeText(
                                                offersPromotionsLine1,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: Color(0xff3D4B65),
                                                    fontSize: 18,
                                                    fontFamily: "Poppins",
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 0, 5, 0),
                                              child: AutoSizeText(
                                                offersPromotionsLine2,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: Color(0xff1DC7E6),
                                                    fontSize: 18,
                                                    fontFamily: "Poppins",
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 15, 5, 0),
                                              child: Card(
                                                  elevation: 3,
                                                  color: Color(0xff293855),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: InkWell(
                                                    onTap: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  BusinessCreativeHomeScreen()));
                                                    },
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              20, 10, 20, 10),
                                                      child: AutoSizeText(
                                                        view.toUpperCase(),
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Poppins",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700),
                                                      ),
                                                    ),
                                                  )),
                                            ),
                                          ],
                                        ),
                                        padding: EdgeInsets.all(5),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: ScreenSize.width(context),
                        height: ScreenSize.height(context) / 4.5,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Card(
                            elevation: 2,
                            color: Color(0xff283F6A),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Stack(
                              children: [
                                Positioned(
                                    top: 20,
                                    right: 20,
                                    child: Container(
                                      height: ScreenSize.height(context) / 5,
                                      child: Image.asset(
                                          "assets/images/social_media_banner.png"),
                                    )),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        margin:
                                            EdgeInsets.fromLTRB(10, 5, 10, 5),
                                        height: ScreenSize.width(context),
                                        width: ScreenSize.height(context),
                                        child: Padding(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    5, 5, 5, 0),
                                                child: AutoSizeText(
                                                  socialMediaLine1,
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      color: Color(0xff54B3FF),
                                                      fontSize: 18,
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    5, 0, 5, 0),
                                                child: AutoSizeText(
                                                  socialMediaLine2,
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 18,
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    5, 15, 5, 0),
                                                child: Card(
                                                    elevation: 3,
                                                    color: Color(0xff1798FF),
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    5))),
                                                    child: InkWell(
                                                      onTap: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        SocialMediaPostsHomeScreen()));
                                                      },
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.fromLTRB(
                                                                20, 10, 20, 10),
                                                        child: AutoSizeText(
                                                          view.toUpperCase(),
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Poppins",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                        ),
                                                      ),
                                                    )),
                                              ),
                                            ],
                                          ),
                                          padding: EdgeInsets.all(5),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                      ),
                      Container(
                        width: ScreenSize.width(context),
                        height: ScreenSize.height(context) / 3.5,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child: Card(
                                  elevation: 2,
                                  color: Color(0xffFF7266),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  child: InkWell(
                                    onTap: () {
                                      // Navigator.push(
                                      //     context,
                                      //     MaterialPageRoute(
                                      //         builder: (context) => Avatar()));
                                      // startTimer();
                                    },
                                    child: Column(
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: Container(
                                            margin: EdgeInsets.all(7),
                                            child: Card(
                                              elevation: 0,
                                              color: Colors.white,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10))),
                                              child: Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.all(16),
                                                child: Image.asset(
                                                  "assets/images/whatsapp.png",
                                                  height: 50,
                                                  width: 50,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            flex: 1,
                                            child: Container(
                                              margin: EdgeInsets.all(8),
                                              alignment: Alignment.center,
                                              child: AutoSizeText(
                                                shareStoreLink,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    fontFamily: "Poppins",
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              flex: 1,
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child: Card(
                                    elevation: 2,
                                    color: Color(0xffFFCA28),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: InkWell(
                                      onTap: () {
                                        showModalBottomSheet(
                                            context: context,
                                            backgroundColor: Colors.white,
                                            /*shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10))),*/
                                            builder: (context) {
                                              return new BottomSheetWhatsappShareWidget();
                                            });
                                      },
                                      child: Column(
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: Container(
                                              margin: EdgeInsets.all(7),
                                              child: Card(
                                                elevation: 0,
                                                color: Colors.white,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.all(16),
                                                  child: Image.asset(
                                                    "assets/images/share_pdf.png",
                                                    height: 50,
                                                    width: 50,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                              flex: 1,
                                              child: Container(
                                                margin: EdgeInsets.all(8),
                                                alignment: Alignment.center,
                                                child: AutoSizeText(
                                                  getCatalogWhatsApp,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14,
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              )),
                                        ],
                                      ),
                                    )),
                              ),
                              flex: 1,
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child: Card(
                                    elevation: 2,
                                    color: Color(0xff64B5F6),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    MyQrsScreen()));
                                      },
                                      child: Column(
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: Container(
                                              margin: EdgeInsets.all(7),
                                              child: Card(
                                                elevation: 0,
                                                color: Colors.white,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.all(16),
                                                  child: Image.asset(
                                                    "assets/images/qr_code.png",
                                                    height: 50,
                                                    width: 50,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                              flex: 1,
                                              child: Container(
                                                margin: EdgeInsets.all(8),
                                                alignment: Alignment.center,
                                                child: AutoSizeText(
                                                  getStoreQr,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14,
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              )),
                                        ],
                                      ),
                                    )),
                              ),
                              flex: 1,
                            ),
                          ],
                        ),
                      ),
                    ])
              : counterView(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Widget counterView() {
    return ScaleTransition(
      scale: _tween.animate(
          CurvedAnimation(parent: _controller, curve: Curves.elasticOut)),
      child: SizedBox(
        height: 100,
        width: 100,
        child: Center(
            child: CircleAvatar(
          child: Text(
            counterText.toString(),
            style: TextStyle(
              fontSize: 30,
            ),
          ),
          backgroundColor: Colors.transparent,
        )),
      ),
    );
  }
}
