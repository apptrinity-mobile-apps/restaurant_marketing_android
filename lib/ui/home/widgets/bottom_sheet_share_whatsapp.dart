import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:marketing/utils/constants.dart';
import 'package:marketing/utils/screen_size.dart';

class BottomSheetWhatsappShareWidget extends StatelessWidget {
  const BottomSheetWhatsappShareWidget() : super();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Align(
          alignment: Alignment.centerRight,
          child: Container(
            margin: EdgeInsets.fromLTRB(0, 10, 8, 0),
            child: InkWell(
              customBorder: new CircleBorder(),
              child: Icon(
                Icons.close,
                size: 30,
                color: Colors.black,
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ),
        ),
        Container(
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: Align(
                    alignment: Alignment.center,
                    child: AutoSizeText(
                      shareFullCatalogWhatsApp,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w600,
                          color: Color(0xff3D4B65)),
                    ),
                  ),
                ),
                flex: 2,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                  child: Image.asset(
                    "assets/images/share_whatsapp.png",
                    height: 100,
                  ),
                ),
                flex: 1,
              ),
            ],
          ),
        ),
        InkWell(
          onTap: () {},
          child: Container(
              width: ScreenSize.width(context),
              margin: EdgeInsets.fromLTRB(8, 10, 8, 10),
              padding: EdgeInsets.all(12),
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xff1DC5E9), Color(0xff1EE1C3)],
                ),
              ),
              child: AutoSizeText(
                getFullCatalog,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.w600),
              )),
        ),
        Container(
          color: Color(0xffF6F8FB),
          width: ScreenSize.width(context),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(8, 5, 8, 5),
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: Image.asset(
                          "assets/images/header_bullet.png",
                          height: 18,
                          width: 10,
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(7, 0, 5, 0),
                          child: AutoSizeText(howItWorks,
                              style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff3D4B65))),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(8, 5, 8, 5),
                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: Image.asset(
                          "assets/images/bullets.png",
                          height: 20,
                          width: 20,
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          child: AutoSizeText(sendFullCatalogWhatsApp,
                              overflow: TextOverflow.visible,
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Color(0xff3D4B65),
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w400)),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(8, 5, 8, 10),
                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          "assets/images/bullets.png",
                          height: 20,
                          width: 20,
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          child: AutoSizeText(forwardToCustomers,
                              overflow: TextOverflow.visible,
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Color(0xff3D4B65),
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w400)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
