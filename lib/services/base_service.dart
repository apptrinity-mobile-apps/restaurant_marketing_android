import 'package:marketing/ui/edit_share/model/offers_promo_post_data_model.dart';

abstract class BaseService {
  // final String baseUrl = "http://3.138.149.52/api/marketing_mobile/";
  final String baseUrl = "http://3.14.187.24/api/marketing_mobile/";
  // final String baseUrl = "http://18.190.55.150/api/marketing_mobile/";

  Future<dynamic> login(String username, String password);

  Future<dynamic> logout(String employeeId, String restaurantId);

  Future<dynamic> favouriteMediaPost(String socialMediaPostsId, String employeeId, String restaurantId);

  Future<dynamic> unFavouriteMediaPost(String socialMediaPostsId, String employeeId, String restaurantId);

  Future<dynamic> viewAllSocialMediaCategories(String employeeId, String restaurantId);

  Future<dynamic> viewAllSocialMediaPosts(String employeeId, String restaurantId);

  Future<dynamic> categoryBasedSocialMediaPosts(String categoriesId, String employeeId, String restaurantId);

  Future<dynamic> viewAllOffersAndPromos(String employeeId, String restaurantId);

  Future<dynamic> viewOfferPromoDetails(String offersPromotionsId, String employeeId, String restaurantId);

  Future<dynamic> viewRestaurantQR(String restaurantId);

  Future<dynamic> savePromoOffer(OffersAndPromoPostData obj);

  Future<dynamic> deletePromoOffer(String offersPromotionsId, String employeeId, String restaurantId);

  Future<dynamic> menus(String restaurantId);

}