import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:marketing/services/base_service.dart';
import 'package:marketing/ui/edit_share/model/offers_promo_post_data_model.dart';
import 'app_exceptions.dart';

class ApiService extends BaseService {
  @override
  Future login(String username, String password) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/mobile_login");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"email": username, "password": password}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future logout(String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/mobile_logout");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode(
              {"employeeId": employeeId, "restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future favouriteMediaPost(
      String socialMediaPostsId, String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/favourites_social_media_post");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "socialMediaPostsId": socialMediaPostsId,
            "employeeId": employeeId,
            "restaurantId": restaurantId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future unFavouriteMediaPost(
      String socialMediaPostsId, String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/unfavourites_social_media_post");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "socialMediaPostsId": socialMediaPostsId,
            "employeeId": employeeId,
            "restaurantId": restaurantId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future viewAllSocialMediaCategories(
      String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_social_media_categories");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode(
              {"employeeId": employeeId, "restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future viewAllSocialMediaPosts(String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_social_media_posts");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode(
              {"employeeId": employeeId, "restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future categoryBasedSocialMediaPosts(
      String categoriesId, String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/categories_basedon_media_posts");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "categoriesId": categoriesId,
            "employeeId": employeeId,
            "restaurantId": restaurantId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future viewAllOffersAndPromos(String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_offers_and_promotions");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode(
              {"employeeId": employeeId, "restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future viewOfferPromoDetails(
      String offersPromotionsId, String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_offers_and_promotions");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "offersPromotionsId": offersPromotionsId,
            "employeeId": employeeId,
            "restaurantId": restaurantId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future viewRestaurantQR(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_qrcode_image");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future savePromoOffer(OffersAndPromoPostData obj) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_offers_and_promotions");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: jsonEncode(obj));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future deletePromoOffer(
      String offersPromotionsId, String employeeId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_offers_and_promotions");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "offersPromotionsId": offersPromotionsId,
            "employeeId": employeeId,
            "restaurantId": restaurantId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future menus(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_menus");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @visibleForTesting
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw InternalServerException(response.body.toString());
      default:
        throw FetchDataException(
            'Error occurred while communication with server with status code : ${response.statusCode}');
    }
  }
}
