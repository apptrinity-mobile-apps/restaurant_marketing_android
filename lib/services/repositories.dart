import 'package:marketing/services/api_services.dart';
import 'package:marketing/services/base_service.dart';
import 'package:marketing/ui/edit_share/model/offers_promo_post_data_model.dart';

/* local repository */
class Repository {
  BaseService _service = ApiService();

  Future<dynamic> userLogin(String username, String password) async {
    dynamic response = await _service.login(username, password);
    return response;
  }

  Future<dynamic> userLogout(String employeeId, String restaurantId) async {
    dynamic response = await _service.logout(employeeId, restaurantId);
    return response;
  }

  Future<dynamic> getSocialMediaCategories(
      String employeeId, String restaurantId) async {
    dynamic response =
        await _service.viewAllSocialMediaCategories(employeeId, restaurantId);
    return response;
  }

  Future<dynamic> getAllSocialMediaPosts(
      String employeeId, String restaurantId) async {
    dynamic response =
        await _service.viewAllSocialMediaPosts(employeeId, restaurantId);
    return response;
  }

  Future<dynamic> favourite(
      String socialMediaPostsId, String employeeId, String restaurantId) async {
    dynamic response = await _service.favouriteMediaPost(
        socialMediaPostsId, employeeId, restaurantId);
    return response;
  }

  Future<dynamic> unFavourite(
      String socialMediaPostsId, String employeeId, String restaurantId) async {
    dynamic response = await _service.unFavouriteMediaPost(
        socialMediaPostsId, employeeId, restaurantId);
    return response;
  }

  Future<dynamic> getPostsBasedOnCategory(
      String categoriesId, String employeeId, String restaurantId) async {
    dynamic response = await _service.categoryBasedSocialMediaPosts(
        categoriesId, employeeId, restaurantId);
    return response;
  }

  Future<dynamic> getAllPromosOffers(
      String employeeId, String restaurantId) async {
    dynamic response =
        await _service.viewAllOffersAndPromos(employeeId, restaurantId);
    return response;
  }

  Future<dynamic> getOfferPromoDetails(
      String offersPromotionsId, String employeeId, String restaurantId) async {
    dynamic response = await _service.viewOfferPromoDetails(
        offersPromotionsId, employeeId, restaurantId);
    return response;
  }

  Future<dynamic> getRestaurantQR(String restaurantId) async {
    dynamic response = await _service.viewRestaurantQR(restaurantId);
    return response;
  }

  Future<dynamic> updatePromoOffer(OffersAndPromoPostData obj) async {
    dynamic response = await _service.savePromoOffer(obj);
    return response;
  }

  Future<dynamic> deletePromoOffer(
      String offersPromotionsId, String employeeId, String restaurantId) async {
    dynamic response = await _service.deletePromoOffer(
        offersPromotionsId, employeeId, restaurantId);
    return response;
  }

  Future<dynamic> menu(String restaurantId) async {
    dynamic response = await _service.menus(restaurantId);
    return response;
  }

}
