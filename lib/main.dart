import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:marketing/ui/business_creatives/viewModel/all_promos_offers_view_model.dart';
import 'package:marketing/ui/edit_share/viewModel/offers_promos_view_model.dart';
import 'package:marketing/ui/home/screens/home_screen.dart';
import 'package:marketing/ui/login/viewModel/login_view_model.dart';
import 'package:marketing/ui/my_qr/viewModel/qr_view_model.dart';
import 'package:marketing/ui/product_details/viewModel/menu_view_model.dart';
import 'package:marketing/ui/social_media_posts/viewModel/social_media_view_model.dart';
import 'package:marketing/ui/splash/splash_screen.dart';
import 'package:marketing/utils/constants.dart';
import 'package:provider/provider.dart';
import 'ui/login/screens/login.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarIconBrightness: Brightness.light,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => LoginViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => SocialMediaCategoriesViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => AllPromosOffersViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => PromosOffersViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => QRViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => MenuViewModel(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: appName,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: SplashScreen(),
        routes: <String, WidgetBuilder>{
          '/HomeScreen': (BuildContext context) => new HomeScreen(),
          '/LoginScreen': (BuildContext context) => new LoginScreen()
        },
      ),
    );
  }
}
